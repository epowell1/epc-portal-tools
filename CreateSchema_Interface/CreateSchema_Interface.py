# This is a sample Python script.
from ordbms_base import ORDBMSBase
import os
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


class SchemaFiles(ORDBMSBase):
    def __init__(self, filepath, str_schema, conn_string):
        self.output_path = filepath
        self.schema = str_schema
        ORDBMSBase.__init__(self, conn_string, 'POSTGRESQL')

    def CreateMV(self):
        filename = 'materialized_views.sql'
        lst_tables = self.get_tables()
        with open(os.path.join(self.output_path, filename), 'w') as f:
            for table in lst_tables:
                strsql = 'create materialized view %s.mv_%s as select ' % (self.schema, table[0])
                strsql += self.get_fields(table[0]) + ' from %s.%s; \n' % (self.schema, table[0])
                f.write(strsql)
        return

    def CreateFT(self, server_name):
        filename = 'foreign_tables.sql'
        lst_tables = self.get_tables()
        with open(os.path.join(self.output_path, filename), 'w') as f:
            for table in lst_tables:
                strsql = "create foreign table %s.%s (" % (self.schema, table[0][3:])
                strsql += self.get_fields(table[0])
                strsql += ") server %s options (schema_name '%s', table_name '%s'); \n" % (server_name, self.schema,
                                                                                               table[0])
                f.write(strsql)
        return

    def get_tables(self):
        tbl_list = self.get_values("select matviewname from pg_catalog.pg_matviews where schemaname = '%s'"
                                   % self.schema)
        return tbl_list

    def get_fields(self, str_table):
        fld_list = self.get_values("select tgis.f_tgis_genepcschemafieldlists('%s', '%s')" % (self.schema, str_table))
        return fld_list[0][0]


if __name__ == '__main__':

    filepath = '../'
    conn_string = "host='dsdevmaster' port='5433' user='postgres' password='postgres' dbname='mactec'"
    obj_db = SchemaFiles(filepath, 'staging', conn_string)
    # obj_db.CreateMV()
    obj_db.CreateFT('epcec2_littlerock')

