from ordbms_base import ORDBMSBase
import os

class IncludedFields(ORDBMSBase):
    def __init__(self, str_schema):
        self.schema = str_schema
        ORDBMSBase.__init__(self, conn_string, 'POSTGRESQL')

    def create_fieldlist(self):
        lst_tables = self.get_tables()
        for table in lst_tables:
            # Check for a spatial column
            spat_col = self.get_values("select f_geometry_column "
                                       "from public.geometry_columns "
                                       "where f_table_schema = '%s' "
                                       "and f_table_name = ltrim('%s','mv_')" % (self.schema, table[0]))
            str_column_config = '{"join_field": "threegisid", "column_include": '
            str_fieldobj = self.get_values("select tgis.f_tgis_get_fieldlist('%s','%s') " % (self.schema, table[0]))
            if spat_col:
                str_fields = str_fieldobj[0][0].replace(',', '","') + '", "shape"'
            else:
                str_fields = str_fieldobj[0][0].replace(',', '","')+'"'
            str_column_config += '["' + str_fields + ']}'
            strsql = "update tgis.table_config set column_config = '%s' where table_name = '%s'; " \
                     % (str_column_config, table[0],)
            print(strsql)
            self.execute(strsql)
        return

    def get_tables(self):
        tbl_list = self.get_values("select matviewname from pg_catalog.pg_matviews where schemaname = '%s'"
                                   % self.schema)
        return tbl_list

    def get_fields(self, str_table):
        fld_list = self.get_values("select tgis.f_tgis_genepcschemafieldlists('%s', '%s')" % (self.schema, str_table))
        return fld_list[0][0]


if __name__ == '__main__':
    # filepath = '../'
    conn_string = "host='dsdevmaster' port='5433' user='postgres' password='postgres' dbname='mactec'"
    obj_db = IncludedFields('staging')
    obj_db.create_fieldlist()

