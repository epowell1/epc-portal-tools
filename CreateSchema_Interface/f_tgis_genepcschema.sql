/* Logic to generate the list of fields to be operated on based on the table level excludions
 * in the table_cnfig table
 * Written By; Eric Powell  Date: 2020/07/13
 */

create or replace function tgis.f_tgis_genepcschemafieldlists(table_name text, table_schema text default 'telecom')
returns text
as 
$tgis$
declare 
	rec record;
	strfldlist text;
	intcnt integer:=0;
begin 
	for rec in 
		execute format('with col_exclude as 
		(with ex_columns as 
		(select (jsonb_array_elements_text(column_config->''column_ignore'')) as col
		from tgis.table_config c where c.table_name = %L)
							select col from ex_columns
							union 
							select f_geometry_column::text as col 
							from public.geometry_columns 
							where f_table_schema = %L 
								and f_table_name = %L)
		select ios.column_name ||'' ''|| ios.data_type as colrec, c.col 
		from information_schema.columns ios
		left join col_exclude c 
		on ios.column_name = c.col		
		where ios.table_schema = %L
		and ios.table_name = %L
		and c.col is null;', table_name, table_schema, table_name, table_schema, table_name)
	loop
		if intcnt = 0 then 
			strfldlist:= rec.colrec;
			intcnt:= intcnt+1;
		else
			strfldlist:= strfldlist||','||rec.colrec;
		end if;
		--raise notice 'Field List: %',strfldlist;
	end loop; 

	-- If no configuration records, pull the entire set of fields from information schema
	if intcnt = 0 then 
		for rec in 
			execute format('select ios.column_name ||'' ''|| ios.data_type as colrec
							from information_schema.columns 
							where table_schema = %L
							and table_name = %L', table_schema, table_name)
		loop
			if intcnt = 0 then 
				strfldlist:= rec.colrec;
				intcnt:= intcnt+1;
			else
				strfldlist:= strfldlist||','||rec.colrec;
			end if;
		end loop;
	end if;
	return strfldlist;
end;
$tgis$
	language plpgsql;
