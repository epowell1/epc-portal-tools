/* Logic to generate the list of fields to be operated on based on the table level excludions
 * in the table_cnfig table
 * Written By; Eric Powell  Date: 2020/07/13
 */

create or replace function tgis.f_tgis_genepcschemafieldlists(table_name text, table_schema text default 'telecom')
returns text
as 
$tgis$
declare 
	rec record;
	strfldlist text;
	intcnt integer:=0;
begin 
	for rec in 
		--Table implmentation
		/*
		execute format('with in_columns as 
		(
			select (jsonb_array_elements_text(column_config->''column_include'')) as col
			from tgis.table_config c where c.table_name = %L
		)
		select ios.column_name ||'' ''|| ios.data_type as colrec, c.col 
		from information_schema.columns ios
		left join in_columns c 
		on ios.column_name = c.col		
		where ios.table_schema = %L
		and ios.table_name = %L', table_name, table_schema, table_name)
		*/
	
		-- Materialized View implementation	
		execute format('with in_columns as 
		(
			select (jsonb_array_elements_text(column_config->''column_include'')) as col
			from tgis.table_config c where c.table_name = ltrim(%L,''mv_'')
		), 
		mv_columns as 
		(
			SELECT a.attname, pg_catalog.format_type(a.atttypid, a.atttypmod) as data_type
			FROM pg_attribute a
			JOIN pg_class t on a.attrelid = t.oid
			JOIN pg_namespace s on t.relnamespace = s.oid
			WHERE a.attnum > 0 
			AND NOT a.attisdropped
			AND t.relname = %L --<< replace with the name of the MV 
			AND s.nspname = %L --<< change to the schema your MV is in 
			ORDER BY a.attnum
		)
		select ios.attname ||'' ''|| ios.data_type as colrec, c.col 
		from mv_columns ios
		left join in_columns c 
		on ios.attname = c.col', table_name, table_schema, table_name)
	loop
		if intcnt = 0 then 
			strfldlist:= rec.colrec;
			intcnt:= intcnt+1;
		else
			strfldlist:= strfldlist||','||rec.colrec;
		end if;
		--raise notice 'Field List: %',strfldlist;
	end loop; 

	-- If no configuration records, pull the entire set of fields from information schema
	if intcnt = 0 then 
		for rec in 
			-- Tbale implementation
			/*execute format('select ios.column_name ||'' ''|| ios.data_type as colrec
							from information_schema.columns 
							where table_schema = %L
							and table_name = %L', table_schema, table_name)
			*/						
		
			--Materialized View implementation
			EXECUTE format('with mv_columns as 
			(
				SELECT a.attname, 
				pg_catalog.format_type(a.atttypid, a.atttypmod) as data_type
				FROM pg_attribute a
				JOIN pg_class t on a.attrelid = t.oid
				JOIN pg_namespace s on t.relnamespace = s.oid
				WHERE a.attnum > 0 
				AND NOT a.attisdropped
				AND t.relname = %L --<< replace with the name of the MV 
				AND s.nspname = %L --<< change to the schema your MV is in 
				ORDER BY a.attnum
			)
			select ios.attname ||'' ''|| ios.data_type as colrec
			from mv_columns as ios', table_schema, table_name)
		loop
			if intcnt = 0 then 
				strfldlist:= rec.colrec;
				intcnt:= intcnt+1;
			else
				strfldlist:= strfldlist||','||rec.colrec;
			end if;
		end loop;
	end if;
	return strfldlist;
end;
$tgis$
	language plpgsql;
