import psycopg2
import os


class ORDBMSBase:
    """
        This class provides a high level interface to execute SQL queries on a database. Database connection information
         is stored in a configuration json, so class only needs the connection string and database platform to create
         the connection objects.
    """
    def __init__(self, conn_string, platform):
        """
        :type platform: basestring
        :type conn_string: basestring
        """
        if platform == 'POSTGRESQL':
            self.connection = psycopg2.connect(conn_string)
            self.dblib = psycopg2
#        elif platform == 'ORACLE':
#            self.connection = cx_oracle.connect(conn_string)
#            self.dblib = cx_oracle
#        else:
#            self.connection = pyodbc.connect(conn_string)
#            self.dblib = pyodbc

    def get_values(self, sql):
        """
        :type sql: basestring
        :rtype: list[tuple]
        """
        try:
            with self.connection as con:
                cur = con.cursor()
                cur.execute(sql)
                # print (sql)
                results = cur.fetchall()
                # print (results)
                cur.close()

        except self.dblib.Error as e:
            print("Error %s:" % e.args[0])
            print(sql)
            raise
            # sys.exit(1)
        return results

    def execute(self, sql):
        """
        :type sql: basestring
        """
        try:
            with self.connection as con:
                cur = con.cursor()
                cur.execute(sql)
                cur.close()

        except self.dblib.Error as e:
            # print("Error %s:" % e.args[0])
            # print sql
            raise e
            # sys.exit(1)
        return

    def execute_sql_script(self, script_name, script_path, lst_replacements):
        """
         :type script_name: basestring
         :type script_path: basestring
         :type lst_replacements: list[tuple]
         :rtype: list[tuple]
         """
        # Generate a list of SQL scripts to execute
        full_sql = ''
        with open(os.path.join(script_path, script_name)) as f:
            lst_lines = f.readlines()
            # print lst_lines
            for line in lst_lines:
                # arcpy.AddMessage(line)
                if len(line.strip()) > 0 and line.strip()[0:2] != '/*' and line.strip()[0:1] != '*':
                    # Split the line at the comment characters
                    lstclauses = line.split('--')
                    # Grab everything left of the split , strip the whitespace
                    if lstclauses[0].strip() is not None:
                        line = lstclauses[0].strip()
                    for item in lst_replacements:
                        if line.find(item[0]) != -1:
                            line = line.replace(item[0], item[1])
                    if script_name[0:6] == '02_tbl':
                        full_sql = full_sql + ' ' + line.strip()
                    else:
                        full_sql = full_sql + ' ' + line.strip() + ' \n'
            # arcpy.AddMessage(full_sql)
            if len(full_sql) > 0:
                if script_name[0:6] == '02_tbl':
                    lst_lines = full_sql.split(';')
                    for line in lst_lines:
                        # Skip whitespace
                        if len(line) > 0:
                            self.execute(line)
                else:
                    self.execute(full_sql)
