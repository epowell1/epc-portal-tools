/* Trtigger to call the tgis.f_tgis_loadogr() function 
 * to load new filegeoedatabase dasta into POstgres
 * written by; Eric Powell Date: 07/06/2020
 */

create trigger after insert on tgis.import_tracking 
 for each statement 
	execute procedure tgis.f_tgis_loadogr();

