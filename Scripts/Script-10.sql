create or replace tgis.f_tgis_runtable_delta (staging_schema text, prod_schema text, table_name text, join_field text )
returns void
as 
$tgis$
declare
	rec  as record;
begin 
	--Create a temp table of the deltas between the loaded data and the prod (telecom) data
	drop table if exists tgis.deltas;
	execute format('create table tgis.deltas as
		select sf.%I as staging_id, 
			tf.%I as prod_id
	from staging.fibercable sf
	full outer join telecom.fibercable tf
	on sf.%I =tf.%I 
	and tgis.f_tgis_hash_row(%L, %L, %L, sf.%I) =
		tgis.f_tgis_hash_row(%L, %L, %L,, tf.%I)
	where sf.%I is null 
	or tf.%I is null',staging_schema, table_name, join_field, join_field, 
							prod_schema, table_name, join_field, join_field);
	-- Records with NULL on right are the deletes, process them first
	for rec in
		select prod_id from tgis.delta where staging_id is null 
	do 
		execute format('delete from %I.%I where %I = %s', table_schema, table_name, rec.prod_id);
	loop;
	--Records with NULL on left are inserts
	for rec in
		select stage_id from tgis.delta where prod_id is null 
	do 
		execute format('insert into %I.%I where %I = %s', table_schema, table_name, rec.prod_id);
	loop;
	return;
end;
$tgis$
 language  plpgsql;
 