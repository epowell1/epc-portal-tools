/* Logic to store old record  valuefrom data table in the audit tracking table in JSON form 
 * excuding non-tracked fields from column_ignore. Called by a delete trigger on all tables in the telecom schema
 * Written By; Eric Powell  Date: 2020/07/06
 */

create or replace function tgis.f_tgis_getfeilds(table_schema text, table_name text)
returns text
as 
$tgis$
declare 
	rec record;
	strfldlist text;
	intcnt integer:=0;
begin 
	for rec in 
		select ios.column_name 
		from information_schema.columns ios
		where ios.column_name not in (select jsonb_array_elements(column_config->'column_ignore')
										from tgis.table_config where table_name = tg_table_name)
		and ios.table_schema = tg_table_schema
		and ios.table_name = tg_table_name
	loop
		if intcnt = 0 then 
			strfldlist:= rec.column_name;
			intcnt:= intcnt+1;
		else
			strfldlist:= strfldlist||','||rec.column_name;
		end if;
		--raise notice 'fields:%',strfldlist;	
	end loop; 
	return strfldlist;
end;
$tgis$
	language plpgsql;
