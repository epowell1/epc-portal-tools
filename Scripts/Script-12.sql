/* Function to import all tables (feature classes) from an ESRI filegeodatabase to 
 * a staging schema in Postgres for further processing by other POstgres functions
 */

create or replace function tgis.f_tgis_createstaging()
returns trigger
as
$tgis$
declare
	target_schema text:='staging';
	tblrec record;
	hdrec record;
	debug_str text;
	create_sql text:='create table %I.%I as
	select * from ogr.%I';
	update_sql text:='alter table %I.%I rename column %I to %I';
	id_field text;
	config_obj jsonb;
begin
	--Build the foreign data wrapper infrastructure
		/* Recipe for using the ogr_fdw extension to mount a remote database schema in
	*  our working database.
	* Script to be run as 'postgres' user
	*/
	create extension if not exists ogr_fdw;
	
	-- Create the Foreign Server
	drop server if exists ogr_import cascade; 
	
	execute format('create server ogr_import
	foreign data wrapper ogr_fdw
	OPTIONS (datasource %L)',new.filename);
	
	create schema if not exists ogr;
	--Assign permissions to the schema to the teleocm user
	grant usage on schema ogr to telecom;
	
	-- Create FOREIGN SCHEMA>
	IMPORT FOREIGN SCHEMA  ogr_all
	    FROM SERVER ogr_import INTO ogr;
	
	grant select on all tables in schema ogr to telecom;
	
	--Create the target schema
	execute format ('create schema if not exists %I', target_schema);

	--Import data into permanent Postgres schema
	for tblrec in
		select table_name from information_schema.tables where table_schema ='ogr'
	loop
		-- Get the configuration record from the config table
		-- execute format ('select column_config->>''join_field'' from tgis.table_config where tablename = %L',tblrec.table_name) into id_field;
		--import table to tgis_config schema
		select format(create_sql,target_schema, lower(tblrec.table_name), tblrec.table_name) into debug_str;
		raise notice '%',debug_str;
		execute format('drop table if exists %I.%I', target_schema, lower(tblrec.table_name));
		execute format (create_sql,target_schema, lower(tblrec.table_name),  tblrec.table_name);
		--normalize column names
		for hdrec in 
			execute format ('select column_name from information_schema.columns where table_schema = %L 
			and table_name = %L', target_schema, lower(tblrec.table_name))
		loop
			if hdrec.column_name::text <> lower(hdrec.column_name)::text then
				--select format(update_sql, target_schema, lower(tblrec.table_name), hdrec.column_name, lower(hdrec.column_name)) into debug_str;
				--raise notice '%',debug_str;
				execute format (update_sql, target_schema, lower(tblrec.table_name)::text, hdrec.column_name::text, lower(hdrec.column_name)::text);
			end if;
		end loop;
	end loop;
	
	--Delete foreign schema and server
	drop server if exists ogr_import cascade; 
	drop schema ogr;
	return new;
end;
$tgis$
	language plpgsql;
	
