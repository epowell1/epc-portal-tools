/* Trtigger to call the tgis.f_tgis_compare_records() function 
 * to load new filegeoedatabase dasta into POstgres
 * written by; Eric Powell Date: 07/06/2020
 */

DROP TRIGGER IF EXISTS trg_fbrcbl_cmp_recs ON tgis.fibercable

create trigger trg_fbrcbl_cmp_recs 
	after insert on tgis.fibercable 
    for each statement
    execute procedure tgis.f_tgis_loadogr();