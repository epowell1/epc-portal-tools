create or replace view tgis.v_marketinfo as
select mi.db_info->"username", 
		mi.db_info->>"password", 
		mi.db_info->>"instance",
		mi.db_info->>"dbname", 
		mi.db_info->>"port",
		mi.filepath,
		v.vendor_name, 
		mi.market
from tgis.market_instance mi 
inner join tgis.vendors v  on 
v.vendor_id = mi.vendor_id_fk;