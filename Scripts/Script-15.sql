create or replace view tgis.v_import_tracking as
select filepath, market, import_time 
from tgis.import_tracking i 
inner join tgis.market_instance m 
on m.market_instance_id = i.market_id_fk;