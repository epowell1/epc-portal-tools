INSERT INTO tgis.import_tracking(filename) values ('/opt/data/pg10/s3/Mastec_LittleRockAR_Bulk_2020-04-22_20-30-36_test_2e.gdb');



Select 
fc.fqn_id,
'Unassociated NFID'::text as Error,
5 as avg_minutes_to_clear
--fc.fqn_id, fc.site_span_nfid, fc.cableplacedactual, n.site_nfid
from
telecom.fibercable fc
left join
telecom.nfid_fqn_associations n
on fc.fqn_id = n.fqn_id
where fc.cableplacedactual is not null and n.site_nfid is null
order by fqn_id;


--NFIDs by FQN_ID
create or replace view telecom.v_nfids_per_fqn_id as
select fqn_id, array_agg(nfa.site_nfid) as associated_nfids, count(*) as nfid_count
from telecom.nfid_fqn_associations nfa 
group by fqn_id;


--FQN_IDS by NFID
drop view telecom.v_fqn_ids_per_nfid;
create or replace view telecom.v_fqn_ids_per_nfid as
select nfa.site_nfid , array_agg(nfa.fqn_id) as fqn_ids, count(*) as fqn_id_count
from telecom.nfid_fqn_associations nfa 
group by nfa.site_nfid ;

-- 
create or replace view telecom.v_fqnid_nfis_count as
select fqn_id, count(*) as nfid_count
from telecom.nfid_fqn_associations nfa 
group by fqn_id;

--NFID Placed 
create or replace view telecom.v_nfid_placed as
with plced as 
(
	select fqn_id 
	from telecom.fibercable 
	where cableplacedactual is not null
)
select nfa.site_nfid, array_agg(nfa.fqn_id ) as fqn_list
from telecom.nfid_fqn_associations nfa 
inner join plced p 
on p.fqn_id = nfa.fqn_id
group by nfa.site_nfid;

-- FQNID Placed Date (Date placed)
create or replace view telecom.v_fibercable_placed_date as
select fqn_id, cableplacedactual
from telecom.fibercable f 
where cableplacedactual is not null;












select distinct inventory_status_code, f.milestone_status , f.status_type from telecom.fibercable f ;
select distinct site_passed_status from telecom.nfid_milestone nm ;


select distinct table_name, column_name from information_schema.columns where column_name like '%status%' and table_schema = 'telecom';

grant select on all tables in schema sde to telecom;

update telecom.fibercable set shape = tgis.f_tgis_simplify_geometry(shape);
select count(*) from telecom.tbl_nfidroute tn ;

