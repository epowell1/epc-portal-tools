/* Trtigger to call the tgis.f_tgis_loadogr() function 
 * to load new filegeoedatabase dasta into POstgres
 * written by; Eric Powell Date: 07/06/2020
 */

DROP TRIGGER IF EXISTS trg_loadogr ON tgis.v_import_tracking;

create trigger trg_loadogr instead of
insert
 	on
    tgis.v_import_trackingg for each row 
    execute procedure tgis.f_tgis_import();
    
create or replace function tgis.f_tgis_import()
returns trigger 
as 
$tgis$
declare 
	marketid integer;
begin 
	--Look up the marketid
	select mi.market_instance_id into marketid 
	from tgis.market_instance mi 
	where mi.market =new.market;

	insert into tgis.import_tracking (import_time, filename, market_id_fk)
	values (now(), new.filename, marketid);
	return new;
end;
$tgis$
	language plpgsql;
