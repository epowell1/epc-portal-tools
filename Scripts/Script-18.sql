create or replace view telecom.v_nfid_status_dates as
select o.sitespannfid, array_agg(f.fqn_id), o.status, o.constructionstartactual, o.cableplacedactual ,o.constructioncompletedactual  from public.oofsegmenttracker o 
inner join telecom.fibercable  f 
on f.nfid = o.sitespannfid
group by o.sitespannfid,  o.constructionstartactual, o.cableplacedactual ,o.constructioncompletedactual, o.status  ;



with nfids as 
(
	select max(f.cableplacedactual) as cableplacedactual, 
	max(f.constructionstartactual) as constructionstartactual, 
	max(f.splicetestactual ) as splicetestactual, 
	f.nfid from telecom.fibercable f
	group by nfid
)
select f.*, vnsd.*  
from telecom.v_nfid_status_dates vnsd 
inner join nfids f 
on f.nfid = vnsd.sitespannfid
where f.cableplacedactual vnsd.cableplacedactual 
or 
f.constructionstartactual <> vnsd.constructionstartactual;
--group by vnsd.sitespannfid,  
--vnsd.constructionstartactual, 
--vnsd.cableplacedactual ,
--vnsd.constructioncompletedactual, 
--vnsd.status,
--f.cableplacedactual,
--f.constructionstartactual,
--f.splicetestactual,
--f.nfid; 


/*
SELECT o.sitespannfid, 
    array_agg(f.fqn_id) AS array_agg,
    o.status,
    o.constructionstartactual,
    o.cableplacedactual,
    o.constructioncompletedactual
   FROM oofsegmenttracker o
     JOIN fibercable f ON f.nfid::text = o.sitespannfid::text
  GROUP BY o.sitespannfid, o.constructionstartactual, o.cableplacedactual, o.constructioncompletedactual, o.status;