create or replace function f_tgis_generate_json()
returns trigger 
as $tgis$
declare 
	
begin 
	--Generate a list of columns to store
	select ios.column_name into 
	from information_schema.columns ios
	where ios.column_name not in (select ci.column_name  
									from tgis.column_ignore ci )
	and ios.table_name = tg_table_name
	and ios.table_schema = tg_table_schema;
	
	
end;
$tgis$
	language plpgsql;

