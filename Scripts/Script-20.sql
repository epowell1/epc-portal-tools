create schema staging;
create schema telecom;
create schema sde;
create schema tgis;

create role sde in group rdsadmin;
create role tgis;
create role telecom;


create server epcec2 foreign data wrapper postgres_fdw options(host '10.0.1.208', port '5432', dbname 'mactec');
create user mapping for telecom server epcec2 options(user 'telecom', password 'telecom');
create foreign table staging.fibercable (fqn_id text, shape geometry) server epcec2 options(schema_name 'telecom', table_name 'mv_fibercable');
create foreign table tgis.import_tracking (fqn_id text, shape geometry) server epcec2 options(schema_name 'tgis', table_name 'import_tracking');