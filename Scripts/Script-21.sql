create server epcec2_config foreign data wrapper postgres_fdw options(host '10.100.15.113', port '5432', dbname 'epc_portal_config');
create user mapping for tgis server epcec2_config options(user 'tgis', password 'tgis');
drop foreign table tgis.import_tracking;
create foreign table tgis.import_tracking (import_time timestamp, 
											filename text, 
											market_id_fk integer, 
											import_id integer) 
server epcec2_config 
options(schema_name 'tgis', table_name 'import_tracking');
ALTER TABLE alter table tgis.import_tracking owner to tgis;