DROP TRIGGER trg_loadogr ON tgis.import_tracking;

create trigger trg_loadogr after
insert
    on
    tgis.import_tracking for each row execute procedure tgis.f_tgis_loadogr();