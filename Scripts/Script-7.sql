create or replace view telecom.v_fibercable_archive as
select  old_rec ->> 'fqn_id' as fqnid,
		old_rec ->> 'workorderid' as workorderid,
		old_rec ->> 'type_name' as type ,
		coalesce (old_rec ->> 'datemodified', old_rec ->> 'datecreated')  as date, 
		sum(st_length(old_rec ->>'shape')) as quantity
		from tgis.audit_records ar 
		where ar.source_table  = 'fibercable'
		group by 
		old_rec ->> 'fqn_id',
		old_rec ->> 'workorderid' ,
		old_rec ->> 'type_name',
		old_rec ->> 'datemodified',
		old_rec ->> 'datecreated';
