/* 
* Function to simplify geometries so that they can be accurately compared 
* to enure that they are the same
*/

create or replace function tgis.f_tgis_simplify_geometry(shape geometry)
returns geometry 
as
$tgis$
declare
	the_shape geometry;

begin
	-- Simplify the multi geoemtries to single geometries
	
	
	-- Run the Douglas-Peuker on the result of above to reduce the number of veriticies
	-- slight risk of missing _small_ changes in geometry.
	
	
	return the_shape;
	
end;
$tgis$
	language plpgsql;