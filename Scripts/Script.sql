select * from us_states us where us.state = 'Tennessee'
order by date;


with increase as
(select date,
       state,
       cases - lag(cases) over (order by date)::integer as increase
from us_states
where state = 'Tennessee')
select us.date, us.cases, i.increase, (i.increase / us.cases) * 100 as rate
from us_states us  
inner join increase i 
on i.state = us.state and 
i.date = us.date;

select  db_info::json#>>'{rds, host}' as host,
		db_info::json#>>'{rds, user}' as user,
		db_info::json#>>'{rds, password}' as password,
		db_info::json#>>'{rds, dbname}' as dbname,
		db_info::json#>>'{rds, port}' as port from tgis.market_instance mi;
		
select  db_info::json#>>'{rds}' from tgis.market_instance mi;


select tgis.f_tgis_genepcschemafieldlists('fiberport');



with in_columns as 
		(select (jsonb_array_elements_text(column_include->'column_include')) as col
		from tgis.table_config c where c.table_name = 'fibercable')
		select ios.column_name ||' '|| ios.data_type as colrec, c.col 
		from information_schema.columns ios
		inner join in_columns c 
		on ios.column_name = c.col		
		where ios.table_schema = 'staging'
		and ios.table_name = 'fibercable';
	
	
	update tgis.table_config set column_include = '{"join_field": "threegisid", "column_include": ["creationuser","datecreated","lastuser","datemodified","cable_category","used_for","type_name","category_name","inventory_status_code","from_sequential","to_sequential","calculatedlength","measuredlength","real_length","placementtype","from_structure","to_structure","reelid","cabletype","sheathtype","coretype","diameter","colorscheme","buffercolorscheme","buffercount","fibersperbuffer","fibercount","numberofribbons","admincount","attenuation","isoddcountfront","installationdate","manufacturer","owner","owner_contact_number","locate_vendor","locate_vendor_contact_number","splice_vendor","splice_vendor_contact_number","comments","hub_entrance","structure1ipid","structure2ipid","projectname","to_equipment_name","from_equipment_name","to_splice_enclosure_name","from_splice_enclosure_name","label_id_text","field_verified_date","field_verified","field_verification_method","protected","fqn_id","capital_project_number","maintenance_project_number","turfarea","site_span_nfid","cluster_ring_nfid","networktype","complements","material_cd","cable_owner","jur","wc","slrg","icon_cable_id","sector_count","nfid","milestone_status","workorderid","createworkorderid","editworkorderid","ipid","segmentid","id","threegisid","source_id","source_table","source_database","globalid","edittrackinguser","edittrackingdate","status_type","model_name","isdeleted","jcpolygonid","tail_length","marketname","cbdname","pole_threegisid","structure_threegisid","fdh_threegisid","prl_number","constructionstartplanned","splicetestactual","constructionstartactual","splicetestestimated","cableplacedestimated","cableplacedactual","vendor_reference_id","marketid","permit3gisid","subtypecode","cable_name","scis_site_code","site_code","groupid","sequence","originating_fqnid","turfid","scis_loc_site_code","startfloor","endfloor", "shape"]}' where table_name = 'fibercable';

update tgis.table_config set column_config = column_include;




select matviewname as table_name from pg_catalog.pg_matviews where schemaname ='staging';
select * from pg_catalog.pg_matviews;

SELECT a.attname,
       pg_catalog.format_type(a.atttypid, a.atttypmod),
       a.attnotnull
FROM pg_attribute a
  JOIN pg_class t on a.attrelid = t.oid
  JOIN pg_namespace s on t.relnamespace = s.oid
WHERE a.attnum > 0 
  AND NOT a.attisdropped
  AND t.relname = 'mv_fiber' --<< replace with the name of the MV 
  AND s.nspname = 'staging' --<< change to the schema your MV is in 
ORDER BY a.attnum;





	with in_columns as 
		(
			select (jsonb_array_elements_text(column_config->'column_include')) as col
			from tgis.table_config c where c.table_name = ltrim('mv_fiber','mv_')
		), 
		mv_columns as 
		(
			SELECT a.attname, pg_catalog.format_type(a.atttypid, a.atttypmod) as data_type
			FROM pg_attribute a
			JOIN pg_class t on a.attrelid = t.oid
			JOIN pg_namespace s on t.relnamespace = s.oid
			WHERE a.attnum > 0 
			AND NOT a.attisdropped
			AND t.relname = 'mv_fiber' --<< replace with the name of the MV 
			AND s.nspname = 'staging' --<< change to the schema your MV is in 
			ORDER BY a.attnum
		)
		select ios.attname ||' '|| ios.data_type as colrec, c.col 
		from mv_columns ios
		left join in_columns c 
		on ios.attname = c.col

	