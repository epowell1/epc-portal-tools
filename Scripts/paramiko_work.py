import base64
import paramiko
from pathlib import Path

def get_rsa(path):
    with open(path, 'r') as f:
        rsa_key = f.readline()
    return rsa_key


def copy_file(s3bucket, rsa_key, ec2_host, userid, filename):
    #Extract the vendor from the FGDB name
    vendor = filename.split('_')[0]

    key = paramiko.RSAKey(data=base64.b64decode(rsa_key))
    client = paramiko.SSHClient()
    client.get_host_keys().add(ec2_host, 'ssh-rsa', key)
    client.connect(ec2_host, username=userid, password='thecheat')

    #Execute the script
    stdin, stdout, stderr = client.exec_command('/opt/scritps/epc_portal_load_fgdb.sh %s, %s, %s' % (s3bucket, filename,
                                                                                                  vendor))
    for line in stdout:
        print('... ' + line.strip('\n'))
    client.close()
    
if __name__=='__main__':
    rsa_key = get_rsa('./id_rsa.pem')
    ec2_host = '10.100.15.113'
    config_folder = './'
        try:
        copy_file(bucket, rsa_key, ec2_host, 'ubuntu', tmp_key)
    except Exception as e:
        print(e)
        print(
            'Error moving *.zip file from bucket to EC2 filesystem'.format(
                key, bucket))
        raise e
