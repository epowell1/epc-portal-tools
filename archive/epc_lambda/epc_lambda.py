from epc_lambda.lambda_db import EC2ORDBMSConnection as Ec2
from epc_lambda.lambda_db import RDSORDBMSConnection as Rds
import uuid
from urllib.parse import unquote_plus
import zipfile


def unzip(s3uri, target_dir):
    """"
    :type s3uri string
    :type target_dir string
    """""

    with zipfile.ZipFile(s3uri, 'r') as zip_ref:
        zip_ref.extractall(target_dir)
    return


def lambda_handler(event, context):
    config_folder = './'
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = unquote_plus(record['s3']['object']['key'])
        tmp_key = key.replace('/', '')
        zip_path = '/tmp/{}{}'.format(uuid.uuid4(), tmp_key)

        # # **********Step 1:  Load Staging Database on EC2 instance
        objordbms = Ec2(config_folder)
        fgdb_name = tmp_key.split('.')[0] + '.gdb'
        # Extract vendor and market from the filename
        conn_info = fgdb_name.split('_')
        rds_con_string = objordbms.get_values("select  mi.db_info::json#>>'{rds}' "
                                              "from tgis.market_instance mi "
                                              "inner join tgis.vendors v on "
                                              "v.vendor_id = mi.vendor_id_fk"
                                              "where mi.market = % and v.s3_bucket = %ds" % (conn_info[1], bucket))

        unzip(zip_path, zip_path)
        # Load the data
        objordbms.load_data(fgdb_name)
        del objordbms
        # # **********Step 2:  Run the deltas / updates on the RDS instance
        objordbms = Rds(rds_con_string[0][0])
        objordbms.compare_datasets()
        del objordbms
    return
