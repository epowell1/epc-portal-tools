/* Logic to store old record  valuefrom data table in the audit tracking table in JSON form 
 * excuding non-tracked fields. Called by a update/delete trigger on all tables in the telecom schema
 * Written By; Eric Powell  Date: 2020/07/06
 */

create or replace function tgis.f_tgis_archiverow(tablename text, tableschrma text)
returns trigger 
as $tgis$
declare 
	rec as record;
	strsql text:= 'insert into tgis.audit_tracking (file_id_fk, process, old_rec, source_table) 
					select %s, %I, row_to_json(%L), %I from %L.%L';
	strfldlist text:='';
	intcnt integer:=0;
 /* 
  * FIX ME: Need a better mechanism to hand fileid to this process than just taking the largest....
  */
	fileid integer;
begin 
	--Generate a query to convert the row to JSON excluding the fields in column_ignore
	select max(file_id) into fileid from tgis.import_tracking it;
	for rec in 
		select ios.column_name 
		from information_schema.columns ios
		where ios.column_name not in (select ci.column_name  
									from tgis.column_ignore ci );
	loop
		if intcnt = 0 then 
			strfldlst:= rec.column_name;
			intcnt:= intcnt+1;
		else
			strfldlist:= strfldlist||','||rec.column_name;
		end if;
		
	end loop;
	execute format(strsql, fileid, tg_op, strfldlist, tg_table_name);
end;
$tgis$
