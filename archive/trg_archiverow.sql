drop trigger if exists trg_archiverow on telecom.fibercable;
create trigger trg_archiverow 
before delete on telecom.fibercable 
for each row
execute procedure tgis.f_tgis_archiverow();