/* Trtigger to call the tgis.f_tgis_importr() function 
 * to update the import_tracking table with Market and Vendot information
 * parsed from filename.
 * written by; Eric Powell Date: 07/06/2020
 */
    
create or replace function tgis.f_tgis_import()
returns trigger 
as 
$tgis$
declare 
	marketid integer;
	arrfilename text[1];
begin 
	--Parse the filename to extract the Market [2] and Vendor [1] values as an array
	with path_end as 
	(
		select (length(new.filename) +1)- position('/' in reverse(new.filename)) as last
	)
	select string_to_array(substring(new.filename from p.last+1 for (length(new.filename)+1) - p.last),'_')  into arrfilename
	from path_end p;
	-- Determine Market ID from filename and hand into the new.market_id_fk varibale
	select coalesce (mi.market_instance_id,999) into new.market_id_fk
	from tgis.market_instance mi 
	inner join tgis.vendors v 
	on mi.vendor_id_fk = v.vendor_id 
	where mi.market = arrfilename[2]
	and v.vendor_name = arrfilename[1];
	-- Get the current time
	select now() into new.import_time;
	return new; 
end;
$tgis$
	language plpgsql;

DROP TRIGGER IF EXISTS trg_import ON tgis.import_tracking;

create trigger trg_import before
insert
 	on
    tgis.import_tracking for each row 
    execute procedure tgis.f_tgis_import();
    
   