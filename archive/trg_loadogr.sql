/* Trtigger to call the tgis.f_tgis_loadogr() function 
 * to load new filegeoedatabase dasta into POstgres
 * written by; Eric Powell Date: 07/06/2020
 */

DROP TRIGGER IF EXISTS trg_loadogr ON tgis.import_tracking;

create trigger trg_loadogr after
insert
 	on
    tgis.import_tracking for each row execute procedure tgis.f_tgis_loadogr();