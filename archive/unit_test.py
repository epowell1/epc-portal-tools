from epc_lambda.lambda_db import EC2ORDBMSConnection as Ec2
from epc_lambda.lambda_db import RDSORDBMSConnection as Rds
import uuid
from urllib.parse import unquote_plus
import zipfile
import os

def unzip(s3uri, target_dir):
    """"
    :type s3uri string
    :type target_dir string
    """""

    with zipfile.ZipFile(s3uri, 'r') as zip_ref:
        zip_ref.extractall(target_dir)
    return

def main(bucket):
    # # TO DO - THIS NEEDS TO BE AN s3 bucket
    config_folder = './'
    ##
    bucket = record['s3']['bucket']['name']
    key = unquote_plus(record['s3']['object']['key'])
    tmp_key = key.replace('/', '')
    zip_path = '/tmp/{}{}'.format(uuid.uuid4(), tmp_key)

    # # **********Step 1:  Load Staging Database on EC2 instance
    # ** Run in Lambda
    objordbms = Ec2(config_folder)
    mount_point = objordbms.get_values('select select filepath '
                                       'from tgis.market_instace '
                                       'where s3_bucket_name = %ds' % bucket)

    rds_con_string = objordbms.get_values("select  mi.db_info::json#>>'{rds}' "
                               "from tgis.market_instance mi "
                               "where s3_bucket_name = %ds" % bucket)

    # Call EC2 with patrh, FGDB Name
    fgdb_name = tmp_key.split('.')[0] + '.gdb'
    # fgdb_path = os.path.join(mount_point, fgdb_name)
    # **************************
    #zip_path = os.path.join(mount_point[0][0], mount_point[0][0])
    unzip(zip_path, zip_path)
    # Load the data and get the RDS connection string
    objordbms.load_data(fgdb_name)
    del objordbms
    # ****************************
    # # **********Step 2:  Run the deltas / updates on the RDS instance
    objordbms = Rds(rds_con_string[0][0])
    objordbms.compare_datasets()
    del objordbms

if __name__ == '__main__':
    main(arn:aws:s3:::ebp-test)