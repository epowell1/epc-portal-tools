from db_objects import EC2ORDBMSConnection as Ec2
from db_objects import RDSORDBMSConnection as Rds
from db_objects import ConfigConnection as Config
import sys

def generate_connection_string(params):
    connection_string = 'host={} user={} password={} port={} dbname={}'.format(params[0],
                                                                                    params[1],
                                                                                    params[2],
                                                                                    params[3],
                                                                                    params[4])
    print(connection_string)
    return connection_string


def main(config_folder, fgdb_name, market, vendor):
    try:
        try:
            obj_config = Config(config_folder)
            print('Get Database Instance Information from epc_portal_config')
            # Extract vendor and market from the filename
            rds_conn = obj_config.get_values("select  db_info::json#>>'{rds, host}' as host, "
                                             "db_info::json#>>'{rds, user}' as user, "
                                             "db_info::json#>>'{rds, password}' as password, "
                                             "db_info::json#>>'{rds, port}' as port, "
                                             "mi.db_name as dbname "
                                             "from tgis.market_instance mi "
                                             "inner join tgis.vendors v "
                                             "on mi.vendor_id_fk = v.vendor_id "
                                             "where mi.market = '%s' and v.vendor_name = '%s'" % (market, vendor))

            ec2_conn = obj_config.get_values("select  db_info::json#>>'{ec2, host}' as host, "
                                             "db_info::json#>>'{ec2, user}' as user, "
                                             "db_info::json#>>'{ec2, password}' as password, "
                                             "db_info::json#>>'{ec2, port}' as port, "
                                             "mi.db_name as dbname "
                                             "from tgis.market_instance mi "
                                             "inner join tgis.vendors v "
                                             "on mi.vendor_id_fk = v.vendor_id "
                                             "where mi.market = '%s' and v.vendor_name = '%s'" % (market, vendor))

            # Load the data into the EC2 market db instance
            import_id = obj_config.get_loadid(fgdb_name, market, vendor)
        except Exception as e:
            print ("Error getting connection strings")
            raise e
        try:
            print('Importing File Goedatabase')
            obj_ec2 = Ec2(generate_connection_string(ec2_conn[0]))
            print('Loading Data from {}'.format(fgdb_name))
            obj_ec2.load_data(fgdb_name)
        except Exception as e:
            print(e)
            print('Error Executing Load OGR Function for {} from vendor {}. '.format(fgdb_name, vendor))
            raise e

        # ****************************
        # # ********** Run the deltas / updates on the RDS instance
        try:
            print('Comparing datasets')
            obj_rds = Rds(generate_connection_string(rds_conn[0]))
            obj_rds.compare_datasets(import_id)
        except Exception as e:
            print(e)
            print(
                'Error Executing RDS Records Compare Function for {} from vendor {}. '.format(fgdb_name, vendor))
            raise e
    except Exception as e:
        raise e
    return


if __name__ == '__main__':
    #   config_folder, fgdb_name, market, vendor
    try:
        main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    except Exception as ex:
        raise ex
