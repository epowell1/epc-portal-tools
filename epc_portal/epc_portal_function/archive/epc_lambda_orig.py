from lambda_db import EC2ORDBMSConnection as Ec2
from lambda_db import RDSORDBMSConnection as Rds
from urllib.parse import unquote_plus
import boto3
from botocore.client import Config
import paramiko
import os
from ssm_parameter_store import SSMParameterStore

config = Config(connect_timeout=15, read_timeout=15)
s3 = boto3.client('s3', config=config)

class BASHException(Exception):
    def __init__(self, str_error):
        message = 'BASH Exception: {}'.format(str_error)
        super().__init__(message)

def ssh_command(ec2_host, userid,  str_command):
    store = SSMParameterStore(prefix='/EPC')
    try:
        print('Start SSH Function')
        # Extract PEM file from SSM Parameter to file in /tmp filespace to feed into paramiko
        with open('/tmp/EC2_RSA.pem', 'w') as f:
            f.write(store['EC2']['EC2_RSA.pem'])
        key_file = '/tmp/EC2_RSA.pem'
        print('Key File:')
        print(key_file)
        key = paramiko.RSAKey.from_private_key_file(key_file)
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("Connecting:")
        client.connect(hostname=ec2_host, username=userid, pkey=key, timeout=10)
        print("Connected")
        # Execute the script
    except Exception as e:
        print('Error establish SSH session to EC2 host:')
        print(e)
        raise e
    try:
        # Execute the script
        print('Executing: {}'.format(str_command))
        stdin, stdout, stderr = client.exec_command(str_command, timeout=30)
        out = stdout.readlines()
        err = stderr.readlines()
        print(out)
        client.close()
    except Exception as e:
        print('Error executing BASH script')
        print(e)
        raise e

    return [err, out]

def lambda_handler(event, context):
    store = SSMParameterStore(prefix='/EPC')
    ec2_host = store['EC2']['EC2_IP']
    print("Before main loop")
    for record in event['Records']:
        print("Inside Main Loop")
        bucket = record['s3']['bucket']['name']
        key = unquote_plus(record['s3']['object']['key'])
        tmp_key = key.replace('/', '')
        path = tmp_key
        #vendor = tmp_key.split('_')[0]
        #if path.suffix.lower() == 'zip':
        if path[-3:].lower() == 'zip':
            try:
#                    fgdb_name = path.with_suffix('.gdb').name
#                market = path.parent.name
                fgdb_name = os.path.join(path.split('.')[0], '.gdb')
                vendor = path.split('_')[0]
                market = path.split('_')[1]
            except Exception as e:
                print(e)
                print(
                    'Key Error for object {} from bucket {}. Make sure key is of format <vendor>_<market>_<date?.zip.'.format(
                        key, bucket))
                raise e

        # # **********Step 1: Call the script on the EC2 instance to copy the zipfile from s3 and unzip it
            try:
                the_ssh_command = '/opt/scripts/epc_portal_load_fgdb.sh %s %s %s' % (bucket, tmp_key, vendor)
                error = ssh_command(ec2_host, 'ubuntu', the_ssh_command)
                print('Return to main function')
                lst_output = error[1]
                # Check the return values for errors and to ensure we get to the 'Complete' in stdout
                if 'Complete\n' not in lst_output:
                    output = ''
                    # print(error)
                    for line in lst_output:
                        output = output + line
                    if output != "":
                        print(output)
                    else:
                        print("There was no output for this command")
                    raise BASHException(error[0])
            except Exception as e:
                print(e)
                print('Error moving {} from {} to EC2 filesystem'.format(tmp_key, bucket))
                raise e
            print('Shell Script executed properly')

            # # **********Step 2:  Load Staging Database on EC2 instance
            # ** Run in Lambda
#            obj_ec2 = Ec2(config_folder)
            print('Get RDS Instance Information from epc_portal_config')
            obj_ec2 = Ec2()
            # Extract vendor and market from the filename
            rds_conn = obj_ec2.get_values("select  db_info::json#>>'{rds, host}' as host, "
                                          "db_info::json#>>'{rds, user}' as user, "
                                          "db_info::json#>>'{rds, password}' as password, "
                                          "db_info::json#>>'{rds, port}' as port "
                                          "mi.db_name as dbname,"
                                          "from tgis.market_instance mi "
                                          "inner join tgis.vendors v "
                                          "on mi.vendor_id_fk = v.vendor_id "
                                          "where mi.market = '%s' and v.vendor_name = '%s'" % (market, vendor))

            # Load the data and get the RDS connection string
            print('Loading Data from {}'.format(fgdb_name))
            try:
                import_id = obj_ec2.load_data(fgdb_name)
            except Exception as e:
                print(e)
                print(
                    'Error Executing Load OGR Function for {} from vendor {}. '.format(key, vendor))
                raise e

            # ****************************
            # # **********Step 3:  Run the deltas / updates on the RDS instance
            try:
                print('Comparing datasets')
                rds_conn_string = "host='%s' user='%s' password='%s' dbname='%s' port='%s'" % (rds_conn[0][0],
                                                                                               rds_conn[0][1],
                                                                                               rds_conn[0][2],
                                                                                               rds_conn[0][3],
                                                                                               rds_conn[0][4])
                print (rds_conn_string)
                obj_rds = Rds(rds_conn_string)
                obj_rds.compare_datasets(import_id)
            except Exception as e:
                print(e)
                print(
                    'Error Executing RDS Records Compare Function for {} from vendor {}. '.format(key, vendor))
                raise e

            # Step 4: Cleanup the staging folder
            print("Cleaning Up")
            try:
                the_ssh_command = 'rm -R /mnt/data/local_data/staging/%s/* ' % vendor
                error = ssh_command(ec2_host, 'ubuntu', the_ssh_command)
                # print('Return to main function')
                lst_output = error[1]
                # Check the return values for errors and to ensure we get to the 'Complete' in stdout
                if len(error[0]) > 0:
                    output = ''
                    # print(error)
                    for line in lst_output:
                        output = output + line
                    if output != "":
                        print(output)
                    else:
                        print("There was no output for this command")
                    raise BASHException(error[0])
            except Exception as e:
                print(e)
                print('Error clearing staging on EC2'.format(tmp_key, bucket))
                raise e
            # print('Shell Script executed properly')
    print('Lambda Function Executed Properly')
    return

