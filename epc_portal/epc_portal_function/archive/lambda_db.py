import json
import os
import psycopg2
# import boto3
from ssm_parameter_store import SSMParameterStore
#
# s3 = boto3.client('s3')

class ORDBMSBase:
    """
        This class provides a high level interface to execute SQL queries on a database. Database connection information
         is stored in a configuration json, so class only needs the connection string and database platform to create
         the connection objects.
    """

    def __init__(self, conn_string):
        """
        :type conn_string: basestring
        """
        self.connection = psycopg2.connect(conn_string)
        self.dblib = psycopg2


    def get_values(self, sql):
        """
        :type sql: basestring
        :rtype: list[tuple]
        """
        try:
            with self.connection as con:
                cur = con.cursor()
                cur.execute(sql)
                # print (sql)
                results = cur.fetchall()
                # print (results)
                cur.close()

        except self.dblib.Error as e:
            print("Error %s:" % e.args[0])
            print(sql)
            raise
            # sys.exit(1)
        return results

    def execute(self, sql):
        """
        :type sql: basestring
        """
        try:
            with self.connection as con:
                cur = con.cursor()
                cur.execute(sql)
                cur.close()

        except self.dblib.Error as e:
            # print("Error %s:" % e.args[0])
            # print sql
            raise e
            # sys.exit(1)
        return

    def execute_sql_script(self, script_name, script_path, lst_replacements):
        """
         :type script_name: basestring
         :type script_path: basestring
         :type lst_replacements: list[tuple]
         :rtype: list[tuple]
         """
        # Generate a list of SQL scripts to execute
        full_sql = ''
        with open(os.path.join(script_path, script_name)) as f:
            lst_lines = f.readlines()
            # print lst_lines
            for line in lst_lines:
                if len(line.strip()) > 0 and line.strip()[0:2] != '/*' and line.strip()[0:1] != '*':
                    # Split the line at the comment characters
                    lstclauses = line.split('--')
                    # Grab everything left of the split , strip the whitespace
                    if lstclauses[0].strip() is not None:
                        line = lstclauses[0].strip()
                    for item in lst_replacements:
                        if line.find(item[0]) != -1:
                            line = line.replace(item[0], item[1])
                    if script_name[0:6] == '02_tbl':
                        full_sql = full_sql + ' ' + line.strip()
                    else:
                        full_sql = full_sql + ' ' + line.strip() + ' \n'
            if len(full_sql) > 0:
                if script_name[0:6] == '02_tbl':
                    lst_lines = full_sql.split(';')
                    for line in lst_lines:
                        # Skip whitespace
                        if len(line) > 0:
                            self.execute(line)
                else:
                    self.execute(full_sql)


class EC2ORDBMSConnection(ORDBMSBase):
    """"
    This class provides a configuration object containing the information needed to connect to a database server
    """""

    #def __init__(self, config_folder, log=None):
    def __init__(self, log=None):
        """"
        :type config_folder string        """""

        self.log = log
        with open(os.path.join(config_folder, "epc_portal_tools.json")) as config_file:
            json_config = json.load(config_file)
            self.json_config = json_config["configuration"]
        conn_string = self.get_connect_string()
        ORDBMSBase.__init__(self, conn_string)

    def get_connect_string(self):
        """
        """
        params = self.json_config["connection"]
        # store = SSMParameterStore(prefix='/EPC')

        str_conn_string = "user='%s' password='%s' host='%s' dbname='%s' port= '%s'" % (params['user_name'],
                                                                                         params['user_password'],
                                                                                         params['hostname'],
                                                                                         params['dbname'],
                                                                                         params['port'])

        # str_conn_string = "user='%s' password='%s' host='%s' dbname='%s' port= '%s'" % (store['EC2']['DB']['Username'],
        #                                                                                 store['EC2']['DB']['Password'],
        #                                                                                 store['EC2']['EC2_IP'],
        #                                                                                 store['EC2']['DB']['DBName'],
        #                                                                                 store['EC2']['DB']['Port'])
        return str_conn_string

    def load_data(self, file_path):
        """"
        :type file_path string
        """""
        import_id = self.get_loadid(file_path)
        self.get_values("select tgis.f_tgis_loadogr('%s')" % file_path)
        return import_id
        # res = res[0][0]
        # con_string = res.replace(':', '=')
        # return con_string

    def get_loadid(self, file_path):
        market = file_path.split('_')[1]
        vendor = file_path.split('_')[0]
        #Dtermine Market ID from Vendor and Market
        market_ids = self.get_values("select market_id "
                                     "from tgis.market_instance mi"
                                     "inner join tgis.vendors v"
                                     "on mi.vendor_id_fk = v.vendor_id"
                                     "where mi.market = '%s' and v.vendor_name = '%s'"
                                     % (market, vendor))
        market_id = market_ids[0]
        self.execute('insert into tgis.import_tracking(import_time, filename, market_id_fk) values (now(), %s, %ds)' % (file_path, market_id))
        #Get the import_id based on max import_id for market_id
        import_id = self.get_values('select max(import_id) from tgis.import_tracking where market_id_fk = %ds' % market_id[0][0])
        return import_id[0][0]

class RDSORDBMSConnection(ORDBMSBase):
    """"
    This class provides a configuration object containing the information needed to connect to a database server
    """""

    def __init__(self, connection_string, log=None):
        """"
        :type connection_string string        """""

        self.log = log
        ORDBMSBase.__init__(self, connection_string)

    def compare_datasets(self, import_id):
        # Get the list of tables to update from the  information schema
        # table_list = self.get_values("select table_name from information_schema where table_schema = '%s'" % 'staging')
        table_list = self.get_values("select table_name from tgis.table_config where load_data = true")
        for table in table_list:
            self.execute("select tgis.f_tgis_copmare_records('%s', %ds)" % (table[0], import_id))
        return

