
from urllib.parse import unquote_plus
import boto3
from botocore.client import Config
import paramiko
import os
from ssm_parameter_store import SSMParameterStore

config = Config(connect_timeout=15, read_timeout=15)
s3 = boto3.client('s3', config=config)

class BASHException(Exception):
    def __init__(self, str_error):
        message = 'BASH Exception: {}'.format(str_error)
        super().__init__(message)

def ssh_command(ec2_host, userid,  str_command):
    store = SSMParameterStore(prefix='/EPC')
    try:
        print('Start SSH Function')
        # Extract PEM file from SSM Parameter to file in /tmp filespace to feed into paramiko
        with open('/tmp/EC2_RSA.pem', 'w') as f:
            f.write(store['EC2']['EC2_RSA.pem'])
        key_file = '/tmp/EC2_RSA.pem'
        print('Key File:')
        print(key_file)
        key = paramiko.RSAKey.from_private_key_file(key_file)
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("Connecting:")
        client.connect(hostname=ec2_host, username=userid, pkey=key, timeout=10)
        print("Connected")
        # Execute the script
    except Exception as e:
        print('Error establish SSH session to EC2 host:')
        print(e)
        raise e
    try:
        # Execute the script
        print('Executing: {}'.format(str_command))
        stdin, stdout, stderr = client.exec_command(str_command, timeout=30)
        out = stdout.readlines()
        err = stderr.readlines()
        print(out)
        client.close()
    except Exception as e:
        print('Error executing BASH script')
        print(e)
        raise e

    return [err, out]

def lambda_handler(event, context):
    store = SSMParameterStore(prefix='/EPC')
    ec2_host = store['EC2']['EC2_IP']
    print("Before main loop")
    for record in event['Records']:
        print("Inside Main Loop")
        bucket = record['s3']['bucket']['name']
        key = unquote_plus(record['s3']['object']['key'])
        tmp_key = key.replace('/', '')
        path = tmp_key
        #vendor = tmp_key.split('_')[0]
        #if path.suffix.lower() == 'zip':
        if path[-3:].lower() == 'zip':
            try:
#                    fgdb_name = path.with_suffix('.gdb').name
#                market = path.parent.name
                fgdb_name = os.path.join(path.split('.')[0], '.gdb')
                vendor = path.split('_')[0]
                market = path.split('_')[1]
            except Exception as e:
                print(e)
                print(
                    'Key Error for object {} from bucket {}. Make sure key is of format <vendor>_<market>_<date?.zip.'.format(
                        key, bucket))
                raise e

        # # **********Step 1: Call the script on the EC2 instance to copy the zipfile from s3 and unzip it
            try:
                the_ssh_command = '/opt/scripts/epc_portal_load_fgdb.sh %s %s %s %s %w' % (bucket, tmp_key, vendor,
                                                                                           market, fgdb_name)
                error = ssh_command(ec2_host, 'ubuntu', the_ssh_command)
                print('Return to main function')
                lst_output = error[1]
                # Check the return values for errors and to ensure we get to the 'Complete' in stdout
                if 'Complete\n' not in lst_output:
                    output = ''
                    # print(error)
                    for line in lst_output:
                        output = output + line
                    if output != "":
                        print(output)
                    else:
                        print("There was no output for this command")
                    raise BASHException(error[0])
            except Exception as e:
                print(e)
                print('Error moving {} from {} to EC2 filesystem'.format(tmp_key, bucket))
                raise e
            print('Shell Script executed properly')
    return

