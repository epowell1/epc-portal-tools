#!/bin/bash
# $1 - s3 bucket name
# $2 - filename
# $3 - vendor
# $4 - market
# $5 - fgdb_name

config_folder='/opt/epc_config/'i

# Copy the ZIP file from the s3 bucket to vendor folder
#aws s3 cp s3://3gis-qa-epc-portal-us-east-1/filename /localstorgae --profile s3UploadUser-QA
rm -R /mnt/data/local_data/staging/${3}/*
echo "Retrieving ${2} from ${3} to /mnt/data/local_data/staging/${3}"
aws s3 cp s3://${1}/${2} /mnt/data/local_data/staging/${3}/${2} --profile s3UploadUser-QA

# Unzip the file
echo "Unzippping ${2}"
unzip /mnt/data/local_data/staging/${3}/${2} -d /mnt/data/local_data/staging/${3}/

# Load the FGDB in to EC2 Staging database and to use to create deltas with RDS instance
python3 /opt/scripts/epc_dataloader/epc_dataload.py $config_folder ${5} ${4} ${3}

#Clean-up
#rm -R /mnt/data/local_data/staging/${3}/*

# Finshed - report completion
echo 'Complete'

