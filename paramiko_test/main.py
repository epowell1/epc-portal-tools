import base64
import paramiko
from pathlib import Path

def get_rsa(path):
    with open(path, 'r') as f:
        rsa_key = f.readline()
    return rsa_key


def copy_file(s3bucket, ec2_host, userid, filename):
    #Extract the vendor from the FGDB name
    vendor = filename.split('_')[0]
    key_file = 'Bechtel_Elastic_Model.pem'
    key = paramiko.RSAKey.from_private_key_file(key_file)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print("Connecting:")
    client.connect(hostname=ec2_host, username=userid, pkey=key)
    print("Connected")
    #Execute the script
    print('/opt/scripts/epc_portal_load_fgdb.sh %s %s %s' % (s3bucket, filename,
                                                                                                  vendor))
    stdin, stdout, stderr = client.exec_command('/opt/scripts/epc_portal_load_fgdb.sh %s %s %s' % (s3bucket, filename,
                                                                                                  vendor))
    print(stdout.read())
    print("Errors")
    print(stderr.read())
    print('Done')
    client.close()

if __name__=='__main__':
    bucket = '3gis-qa-epc-portal-us-east-1'
    tmp_key = 'mastec_littlerock_test.zip'
    ec2_host = '54.162.94.170'
    # ec2_host = '10.100.15.113'
    config_folder = './'
    try:
        copy_file(bucket, ec2_host, 'ubuntu', tmp_key)
    except Exception as e:
        print(e)
       # print(
       #     'Error moving *.zip file from bucket to EC2 filesystem'.format(
       #         key, bucket))
        raise e