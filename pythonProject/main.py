import paramiko

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

class BASHException(Exception):
    def __init__(self, str_error):
        message = 'BASH Exception: {}'.format(str_error)
        super().__init__(message)


def ssh_command(ec2_host, userid,  str_command):
    #store = SSMParameterStore(prefix='/EPC')
    try:
    #    print('Start SSH Function')
    #    # Extract PEM file from SSM Parameter to file in /tmp filespace to feed into paramiko
    #    with open('/tmp/EC2_RSA.pem', 'w') as f:
    #        f.write(store['EC2']['EC2_RSA.pem'])
        key_file = '/home/ebpowell/.ssh/ssh_keys/EC2_RSA.pem'
        print('Key File:')
        print(key_file)
        key = paramiko.RSAKey.from_private_key_file(key_file)
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("Connecting:")
        client.connect(hostname=ec2_host, username=userid, pkey=key, timeout=10)
        print("Connected")
    except Exception as e:
        print('Error establish SSH session to EC2 host:')
        print(e)
        raise e
    try:
        # Execute the script
        print('Executing: {}'.format(str_command))
        stdin, stdout, stderr = client.exec_command(str_command, timeout=30)
        out = stdout.readlines()
        err = stderr.readlines()
        print(out)
        client.close()
    except Exception as e:
        print('Error executing BASH script')
        print(e)
        raise e

    return [err, out]

if __name__ == '__main__':
    bucket = 'epcportalfunction-s3bucket-ix92oxxm6prd'
    tmp_key = 'Mastec_LittleRockAR_Bulk_2020-04-22_20-30-36.zip'
    vendor = 'Mastec'
    ec2_host = '54.162.94.170'
    try:
        the_ssh_command = '/opt/scripts/epc_portal_load_fgdb.sh %s %s %s' % (bucket, tmp_key, vendor)
        error = ssh_command(ec2_host, 'ubuntu', the_ssh_command)
        print('Return to main function')
        lst_output = error[1]
        # Check the return values for errors and to ensure we get to the 'Complete' in stdout
        if 'Complete\n' not in lst_output:
            output = ''
            # print(error)
            for line in error[1]:
                output = output + line
            if output != "":
                print(output)
            else:
                print ("There was no output for this command")
            #print('Error:{}'.format(error[0]))
            raise BASHException(error[0])
    except Exception as e:
        print(e)
        print(
            'Error moving {} from {} to EC2 filesystem'.format(tmp_key, bucket))
        raise e
    print('Shell Script executed properly')