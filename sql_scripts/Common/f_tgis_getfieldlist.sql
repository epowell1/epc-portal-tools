/* Logic to generate the list of fields to be operated on based on the table level excludions
 * in the table_cnfig table
 * Written By; Eric Powell  Date: 2020/07/13
 */

create or replace function tgis.f_tgis_get_fieldlist(table_schema text, table_name text)
returns text
as 
$tgis$
declare 
	rec record;
	strfldlist text;
	intcnt integer:=0;
begin 
	for rec in 
	execute format('select (jsonb_array_elements_text(column_config->''column_include'')) as column_name
					from tgis.table_config c
					 where c.table_name = %L', table_name)
		
	loop
		if intcnt = 0 then 
			strfldlist:= rec.column_name;
			intcnt:= intcnt+1;
		else
			strfldlist:= strfldlist||','||rec.column_name;
		end if;
		--raise notice 'Field List: %',strfldlist;
	end loop; 
	-- If no configuration records, pull the entire set of fields from information schema
	if intcnt = 0 then 
		for rec in 
			execute format('select column_name 
							from information_schema.columns 
							where table_schema = %L
							and table_name = %L', table_schema, table_name)
		loop
			if intcnt = 0 then 
				strfldlist:= rec.column_name;
				intcnt:= intcnt+1;
			else
				strfldlist:= strfldlist||','||rec.column_name;
			end if;
		end loop;
	end if;
	return strfldlist;
end;
$tgis$
	language plpgsql;
