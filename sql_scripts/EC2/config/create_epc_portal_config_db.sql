--Run on new DB instance as postgres user;
create role tigs with login password 'tgis';
create schema tgis;
CREATE SCHEMA staging;
alter schema tgis owner to tgis;
alter schema staging owner to tgis;
CREATE extension ogr_fdw;
GRANT USAGE ON ogr_fdw TO tgis;
GRANT CREATE ALL ON database littlerock TO tgis;

CREATE TABLE tgis.import_tracking (
	file_id serial NOT NULL,
	import_time timestamp NOT NULL,
	filename varchar NOT NULL,
	market_id_fk int4 NULL,
	CONSTRAINT import_tracking_pk PRIMARY KEY (file_id)
);

-- tgis.import_tracking foreign keys

ALTER TABLE tgis.import_tracking ADD CONSTRAINT import_tracking_fk FOREIGN KEY (market_id_fk) REFERENCES tgis.market_instance(market_instance_id);

-- assign owner 
alter table tgis.import_tracking  owner to tgis;

CREATE TABLE tgis.market_instance (
	market_instance_id serial NOT NULL,
	market varchar NOT NULL,
	vendor_id_fk int4 NOT NULL,
	db_name text NOT NULL,
	CONSTRAINT market_instance_pk PRIMARY KEY (market_instance_id)
);


-- tgis.market_instance foreign keys

ALTER TABLE tgis.market_instance ADD CONSTRAINT market_instance_fk FOREIGN KEY (vendor_id_fk) REFERENCES tgis.vendors(vendor_id);

-- assign owner

alter table tgis.market_instance  owner to tgis;


CREATE TABLE tgis.vendors (
	vendor_id serial NOT NULL,
	vendor_name varchar NOT NULL,
	db_info jsonb not null,
	CONSTRAINT vendors_pk PRIMARY KEY (vendor_id)
);

-- assign owner

alter table tgis.vendors owner to tgis;

CREATE TABLE tgis.table_config (
	table_config_id serial NOT NULL,
	table_name varchar NOT NULL,
	column_config jsonb NOT NULL,
	load_table bool NULL,
	CONSTRAINT table_config_pk PRIMARY KEY (table_config_id)
);

-- assign owner

alter table tgis.table_config owner to tgis;
