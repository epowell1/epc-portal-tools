--run as postgres user 
CREATE ROLE tgis WITH login password 'tgis';
CREATE SCHEMA staging;
CREATE SCHEMA tgis;

ALTER SCHEMA staging owner TO tgis;
ALTER SCHEMA tgis owner TO tgis;


--create objects in the tgis schema (2 tables, import_tracking and table_config)

create server epcec2_config foreign data wrapper postgres_fdw options(host '10.100.15.113', port '5432', dbname 'epc_portal_config');
create user mapping for tgis server epcec2_config options(user 'tgis', password 'tgis');
drop foreign table tgis.import_tracking;
create foreign table tgis.import_tracking (import_time timestamp, 
											filename text, 
											market_id_fk integer, 
											import_id
											) 
server epcec2_config 
options(schema_name 'tgis', table_name 'import_tracking');
ALTER TABLE tgis.import_tracking owner to tgis;

CREATE TABLE tgis.table_config (
	table_config_id serial NOT NULL,
	table_name varchar NOT NULL,
	column_config jsonb NOT NULL,
	CONSTRAINT table_config_pk PRIMARY KEY (table_config_id)
);

alter table tgis.table_config owner to tgis;


create foreign table tgis.market_instance (market_instance_id integer, 
											market text, 
											vendor_id_fk integer, 
											db_name text
											) 
server epcec2_config 
options(schema_name 'tgis', table_name 'market_instance');

--Asign ownership

ALTER TABLE tgis.market_instance owner to tgis;

-- Set read-only

revoke insert, update, delete on table tgis.market_instance from tgis;

create foreign table tgis.vendors (vendor_id integer, 
					vendor_name text,
					db_info jsonb
											) 
server epcec2_config 
options(schema_name 'tgis', table_name 'vendors');

--Assign ownership

ALTER TABLE tgis.vendors owner to tgis;

-- Set read-only

revoke insert, update, delete on table tgis.vendors from tgis;

