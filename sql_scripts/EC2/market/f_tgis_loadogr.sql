

create or replace function tgis.f_tgis_loadogr(file_name text, base_path text default '/opt/data/')
returns void
as
$tgis$
declare
	target_schema text:='staging';
	tblrec record;
	insert_sql_1 text:='insert into %I.%I (fid, ';
	insert_sql_2 text:=') select fid, ';
	insert_sql_3 text:=' from ogr.%I';
	config_obj jsonb;
	spatial_col text;
	strfldlist text;
	strsql text;
	connection_string text;
	file_path text;
begin

	--Build the foreign data wrapper infrastructure
		/* Recipe for using the ogr_fdw extension to mount a remote database schema in
	*  our working database.
	* Script to be run as 'postgres' user
	*/
	
	-- Get the filepath
	file_path = base_path || file_name;
	
	create extension if not exists ogr_fdw;
	
	-- Create the Foreign Server
	drop server if exists ogr_import cascade; 
	
	execute format('create server ogr_import
	foreign data wrapper ogr_fdw
	OPTIONS (datasource %L)',file_path);
	
	create schema if not exists ogr;
	--Assign permissions to the schema to the teleocm user
	grant usage on schema ogr to tgis;
	
	-- Create FOREIGN SCHEMA>
	IMPORT FOREIGN SCHEMA  ogr_all
	    FROM SERVER ogr_import INTO ogr;
	
	grant select on all tables in schema ogr to tgis;
	
	/*
	--Create the target schema
	execute format ('create schema if not exists %I', target_schema);
	*/
	--Import data into permanent Postgres schema
	for tblrec in
--		select table_name from information_schema.tables where table_schema ='ogr' and table_name not like 'i%'
		SELECT table_name FROM tgis.table_config WHERE load_table = TRUE
	loop
		--Generate the fieldlist
		select tgis.f_tgis_get_fieldlist(target_schema,tblrec.table_name) into strfldlist;
		--Load the data	
		raise notice 'Schema, Table: %,%', target_schema, tblrec.table_name;
	
		--Purge the target table 
		execute format ('delete from %I.%I', target_schema, tblrec.table_name);
	
		--Load the data
		strsql = insert_sql_1||strfldlist||insert_sql_2||strfldlist||insert_sql_3;
		raise notice 'Query:%',strsql;
		execute format(strsql, target_schema, tblrec.table_name, tblrec.table_name);
		--Refresh the materialized view
		execute format('REFRESH MATERIALIZED VIEW staging.mv_%I with data',tblrec.table_name);
	end loop;
	
	--Delete foreign schema and server
	drop server if exists ogr_import cascade; 
	drop schema ogr;
	return;
end;
$tgis$
	language plpgsql;
	
