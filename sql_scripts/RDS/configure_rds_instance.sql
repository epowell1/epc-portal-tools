create database littlerock;

create schema staging;
create schema telecom;
create schema sde;
create schema tgis;

create extension postgis;
create extension postgres_fdw;
create extension pgrouting;

create role sde with login password '3g!s' in group rds_superuser;
create role tgis with password 'tgis' login;
create role telecom with password 'telecom' login;

--grant usage on schema tgis to telecom;
--grant usage on schema staging to telecom;

grant usage on schema telecom to tgis;
grant usage on schema staging to tgis;
grant usage on schema sde to tgis;
ALTER SCHEMA tgis SET owner TO tgis;
ALTER SCHEMA sde SET owner TO sde;
ALTER SCHEMA telecom SET owner TO telecom;
grant select on all tables in schema staging to tgis;
grant usage on schema telecom to sde;
grant select, insert,update,delete on all tables in schema telecom to tgis;
grant usage on schema sde to telecom;
grant select on all tables in schema sde to telecom;
DROP server IF EXISTS epcec2_market cascade;
DROP server IF EXISTS epcec2_config cascade;
create server epcec2_market foreign data wrapper postgres_fdw options(host '10.100.15.113', port '<vendor port>', dbname '<market>');
create server epcec2_config foreign data wrapper postgres_fdw options(host '10.100.15.113', port '5432', dbname 'epc_portal_config');
create user mapping for tgis server epcec2_config options(user 'tgis', password 'tgis');
create user mapping for tgis server epcec2_market options(user 'tgis', password 'tgis');
GRANT USAGE ON FOREIGN server epcec2_market TO tgis;
GRANT USAGE ON FOREIGN server epcec2_config TO tgis;

--Import the staging tables

-- create foreign table staging.fibercable (fqn_id text, shape geometry) server epcec2_littlerock options(schema_name 'telecom', table_name 'mv_fibercable');


