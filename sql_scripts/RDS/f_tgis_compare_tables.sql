DROP function IF EXISTS tgis.f_tgis_compare_records (src_table_name text);
create or replace function tgis.f_tgis_compare_records (src_table_name text, import_id integer)
returns void
as 
$tgis$
declare
	rec  record;
	strsql_c1 text:= 'insert into %I.%I (objectid, ';
	strsql_c2 text:=') select sde.next_rowid(%L,%L), ';
	strsql_c3 text:=' from %I.%I where %I=%L';
	strsql  text;
	strfldlist text;
	staging_schema text:='staging';
	prod_schema text:='telecom';
	join_field text;
	spatial_col text;
begin 
	-- Get the join field from the table_config table
	execute format('select column_config->>''join_field'' as col
		from tgis.table_config c where c.table_name = %L ', src_table_name) into join_field;
	--Generate the fieldlist
	execute format ('select tgis.f_tgis_get_fieldlist(%L,%L)', staging_schema, src_table_name) into strfldlist;	
/*	-- Get spatial column
	execute format('select f_geometry_column 
				from public.geometry_columns 
				where f_table_schema = %L 
					and f_table_name = %L', staging_schema, src_table_name) into spatial_col;
*/
	--Create a temp table of the deltas between the loaded data and the prod (telecom) data
	drop table if exists tgis.deltas;
	execute format('create table tgis.deltas as
				with stage as 
				(select threegisid, tgis.f_tgis_hash_row(%L, %L, %L, %I) as stage_hash from staging.%I),
				prod as 
				(select threegisid, tgis.f_tgis_hash_row(%L, %L, %L, %I) as prod_hash from telecom.%I)
				select sf.%I as staging_id, 
						tf.%I as prod_id
				from stage sf full outer join prod tf
				on sf.%I =  tf.%I
				where sf.stage_hash <> tf.prod_hash
				or sf.stage_hash is null
				or tf.prod_hash is null',
						staging_schema, src_table_name, join_field, join_field, src_table_name, 
						prod_schema, src_table_name, join_field, join_field, src_table_name,
						join_field, 
						join_field, 
						join_field, join_field);
	-- Records with NULL on right are the deletes, process them first
	for rec in
		select prod_id from tgis.deltas where staging_id is null 
	loop
		select tgis.f_tgis_archiverow(src_table_name, rec.prod_id, import_id, prod_schema, join_field);
--		execute format('delete from %I.%I where %I = %L', prod_schema, src_table_name, join_field, rec.prod_id);
	end loop;
	--Records with NULL on left are inserts
	for rec in
		select staging_id from tgis.deltas where prod_id is null 
	loop 
/*		if spatial_col is not null then 
			strsql = strsql_c1||' %I, '||strfldlist||strsql_c2||' %I,' ||strfldlist||strsql_c3;
			raise notice 'Spatial Query:%',strsql;
			execute format(strsql, prod_schema, src_table_name, spatial_col, prod_schema, src_table_name, spatial_col, staging_schema, src_table_name, join_field, rec.staging_id);
		else
*/
			strsql = strsql_c1||strfldlist||strsql_c2||strfldlist||strsql_c3;
			raise notice 'Query:,%',strsql;
			execute format(strsql, prod_schema, src_table_name, prod_schema, src_table_name, staging_schema, src_table_name, join_field, rec.prod_id);
--		end if;
	end loop;
	return;
end;
$tgis$
 language  plpgsql;
 