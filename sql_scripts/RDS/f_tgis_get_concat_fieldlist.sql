CREATE OR REPLACE FUNCTION tgis.f_tgis_get_concat_fieldlist(table_schema text, table_name text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
	rec record;
	strfldlist text;
	intcnt integer:=0;
begin 
	for rec in 
		execute format('with in_columns as 
		(
			select (jsonb_array_elements_text(column_config->''column_include'')) as col
			from tgis.table_config c where c.table_name = %L
		)
		select ios.column_name, ios.data_type 
		from information_schema.columns ios
		inner join in_columns c 
		on ios.column_name = c.col		
		where ios.table_schema = %L
		and ios.table_name = %L', table_name, table_schema, table_name)
	loop
			if intcnt = 0 then 
				-- If numeric, cast to numeric with 2 decimal points to normalize
				if rec.data_type::text = 'numeric' or rec.data_type::text ='double precision' then 
					strfldlist:= 'coalesce(('||rec.column_name'||::numeric(38,8))::text,''blah'')::text';
				--Round dates to nearest minute - dates should not be only thing in record to change
				elseif rec.data_type::text like 'timestamp%' then 
					strfldlist:= 'date_trunc(''day'', '||rec.colunm_name||')::text';
				elseif rec.data_type::text like 'USER%' then 
					strfldlist:= 'coalesce(st_astext(tgis.f_tgis_simplify_geometry('||rec.column_name||')), ''blah'')::text';
				else
					strfldlist:= 'coalesce('||rec.column_name||'::text,''<null>'')::text';
				end if;
				intcnt:= intcnt+1;
			else
				if rec.data_type::text = 'numeric' or rec.data_type::text ='double precision' then 
					strfldlist:= strfldlist||'||coalesce(('||rec.column_name||'::numeric(38,8))::text,''blah'')::text';
				elseif rec.data_type::text like 'timestamp%' then 
					strfldlist:= strfldlist||'||date_trunc(''day'', '||rec.column_name||')::text';
				elseif rec.data_type::text like 'USER%' then 
					strfldlist:= 'coalesce(st_astext(tgis.f_tgis_simplify_geometry('||rec.column_name||')), ''blah'')::text';
				else
					strfldlist:= strfldlist||'||coalesce('||rec.column_name||'::text,''<null>'')::text';
				end if;
			end if;
		--raise notice 'Field List: %',strfldlist;
	end loop; 
	return strfldlist;
end;
$function$
;
