/* function to convert subset of fields in a database row to a md5 hash
 * for quick comparison of records from disparate sources
 * written by: Eric Powell	Date: 2020/07/09
 */

create or replace function tgis.f_tgis_hash_row (table_schema text, table_name text, id_column text, id_value text)
returns text 
as 
$tgis$
declare 
	hash text;
	rec record;
	strsql text;
/*	strsql_sel text:= 'SELECT md5(CAST((f) AS text))
					FROM  (select ';
	strsql_from text:= ' from %I.%I
							where %I = %L) f'; */
	strsql_sel text:= 'SELECT md5(';
	strsql_from text:= ') from %I.%I
							where %I = %L';						
    fld_list text;
	strfldlist text:='';
	spatial_col text;
begin 
	--Determine if table has a spatial column from the public.geometry_columns view
/*					from public.geometry_columns 
					where f_table_schema = %L 
						and f_table_name = %L', table_schema, table_name) into spatial_col;
						*/
	--Generate the fieldlist
--	select tgis.f_tgis_get_fieldlist(table_schema,table_name) into strfldlist;
	select tgis.f_tgis_get_concat_fieldlist(table_schema,table_name) into strfldlist;
/*	if spatial_col is not null then 
		strsql = strsql_sel||' st_astext(tgis.f_tgis_simplify_geometry(%I))||'||strfldlist||strsql_from;	
		raise notice 'Spatial Query: %, %,%,%,%,%', strsql,spatial_col, table_schema, table_name, id_column, id_value;
		execute format(strsql, spatial_col, table_schema, table_name, id_column, id_value) INTO hash;
	else */
		strsql = strsql_sel||strfldlist||strsql_from;	
		--raise notice 'Non-Spatial Query: %', strsql;
		execute format(strsql, table_schema, table_name, id_column, id_value) INTO hash;
--	end if;

	return hash;
end;
$tgis$	
 language plpgsql;
	


