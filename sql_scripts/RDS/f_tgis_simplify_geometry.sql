/* 
* Function to simplify geometries so that they can be accurately compared 
* to enure that they are the same
*/

create or replace function tgis.f_tgis_simplify_geometry(shape geometry)
returns geometry 
as
$tgis$
declare
	the_shape geometry;
	geo_type text;

begin
	--Get the geometry type from the input value 
	select st_geometrytype(shape) into geo_type; 
	
	-- Simplify the multi geoemtries to single geometries
	--Use different functions based on Geometry type
	--	if geometry is not multipart, skip this step	
	if upper(geo_type) like '%MULTI%' then
	
		if lower(geo_type) like '%line%' then 
			--Multilinestring - try to use st_linemerge
			select st_linemerge(shape) into the_shape;
		else
			--Multipoint / multipoloygon - use ST_DUMP to explode the geoemtry 
			-- count the parts....if one, extract the geometry else???
			with dmp_path as
			(
				select threegisid, (st_dump(shape)).path[1] 
				from staging.fibercable
			)
			SELECT (st_dump(shape)).geom into the_shape
			from staging.fibercable f
			inner join dmp_path p 
			on p.threegisid = f.threegisid
			where p.path = 1;
			--Exceptiion - multipart that actually had multiple parts - cache record in 
			--data error table
			
		end if;
	else
		the_shape:=shape;
	end if;
	-- Run the Douglas-Peuker on the result of above to reduce the number of veriticies
	-- Using 0.5m tolerance
	-- slight risk of missing _small_ changes in geometry.
	return st_simplify(the_shape, 0.5);
end;
$tgis$
	language plpgsql;