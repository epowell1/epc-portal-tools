/* Logic to store old record  valuefrom data table in the audit tracking table in JSON form 
 * excuding non-tracked fields from column_ignore. Called by a delete trigger on all tables in the telecom schema
 * Written By; Eric Powell  Date: 2020/07/06
 */

create or replace function tgis.f_tgis_archiverow(table_name text, 
													prod_id text, 
													import_id integer, 
													join_field text, 
													table_schema text DEFAULT 'telecom')
returns void
as 
$tgis$
declare 
	rec record;
	strsql text;
	strsql_sel text:= 'insert into tgis.audit_records (file_id_fk, old_rec, source_table) 
					select %s as file_id_fk, row_to_json(f), %L as source_table 
					from (select ';
	strsql_from text:=' from %I.%I 
					where %I = %L) f';
	strfldlist text;
	fileid integer;
	spatial_col text;
begin 
	--Generate a query to convert the row to JSON excluding the fields in column_ignore
	/* 
  * FIX ME: Need a better mechanism to hand fileid to this process than just taking the largest....
  */
--	select max(file_id) into fileid from tgis.import_tracking it;
 /* */
/*	--Determine if table has a spatial column from the public.geometry_columns view
	execute format('select f_geometry_column 
					from public.geometry_columns 
					where f_table_schema = %L 
						and f_table_name = %L', tg_table_schema, tg_table_name) into spatial_col;
*/
	--Generate the fieldlist
	select tgis.f_tgis_get_fieldlist(table_schema,table_name) into strfldlist;
/*	if spatial_col is not null then 
		strsql = strsql_sel||' tgis.f_tgis_simplify_geometry(%I),' ||strfldlist||strsql_from;	
--		execute format(strsql,  table_schema, table_name, id_column, id_value);
		execute format(strsql,  fileid,  tg_table_name, spatial_col, tg_table_schema, tg_table_name, old.objectid);
	else
*/
		strsql = strsql_sel||strfldlist||strsql_from;	
		raise notice 'Query:,%',strsql;
--		execute format(strsql, table_schema, table_name, id_column, id_value);
		execute format(strsql, import_id,  table_name,  table_schema, table_name, join_field, prod_id);
		execute format('delete from %I.%I where %I = %L', table_schema, table_name, join_field, prod_id);
--	end if;
	return;
end;
$tgis$
	language plpgsql;
