/* Recipe for using the ogr_fdw extension to mount a ESRI Filegeodatabase database
 *  schema in our working database.
* Script to be run as 'postgres' user
*/

--Create the extension

create extension if not exists ogr_fdw;

-- Create the Foreign Server

create server tgis_mastec_svr
foreign data wrapper ogr_fdw
OPTIONS (datasource '/opt/data/Mastec.gdb');

--Assign permissions to the schema to the teleocm user
CREATE SCHEMA IF NOT EXISTS tgis_mastec;
grant usage on schema tgis_mastec to telecom;
-- Create FOREIGN SCHEMA>
IMPORT FOREIGN SCHEMA ogr_all
    FROM SERVER tgis_mastec_svr
    INTO tgis_mastec;

grant select on all tables in schema tgis_mastec to telecom;

