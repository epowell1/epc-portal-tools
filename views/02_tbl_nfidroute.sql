ALTER TABLE if exists telecom.tbl_nfidroute DROP column geom;
drop table if exists telecom.tbl_nfidroute;
create table if not exists telecom.tbl_nfidroute 
(objectid serial,
edge_id integer,
source integer,
target integer);

SELECT AddGeometryColumn ('telecom','tbl_nfidroute','geom',3857,'LINESTRING',2);
ALTER TABLE telecom.tbl_nfidroute owner to telecom;
