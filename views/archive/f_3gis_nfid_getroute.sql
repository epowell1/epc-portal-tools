--drop function telecom.f_3gis_getroute(integer, text,text, integer);
--drop function if exists telecom.f_3gis_getroute(integer, text,text, text, integer);
--Create the pgrouting topology and the pgr_vertices table
--Create route topology

create or replace function telecom.f_3gis_getroute(segment_oid integer, srcschema text default 'telecom', l_edgetable text default 'fibercable', edge_parent text default 'fibercable', tolerance integer default 10)
returns
table (fqn_id varchar(50), site_span_nfid varchar(50), edge_id integer,  seq integer)
as
$threegis$
declare
	segmentedgeid integer;
	l_site_span_nfid varchar(50);
	hubedge_row record;
	srcnode_row record;
	hubnode_row record;
	int_run integer:=0;
	hub_edgeid integer;
	result_cnt integer:=0;
BEGIN
	--Create a temporary table to house the solve results for the impacted scenarios
	drop table if exists tbl_routeresults;
	CREATE TABLE IF NOT EXISTS tbl_routeresults (oid serial, run integer, hubedge_id integer, segedge_id integer, edge_id integer, seq integer);
	--Determine the spans site_span_nfid
	execute format ('select site_span_nfid::varchar(50) from %I.%I where objectid = %s', srcschema, edge_parent, segment_oid) into l_site_span_nfid;
	-- Iterate over the potential segments (within 300 feet of the hub, having dangels) and try to solve the route
	FOR hubedge_row in 
		SELECT a.edge_id, a.hoid FROM f_3gis_gethubsegment(segment_oid) a	
	loop
		-- IF a starting cranhub id is provided by gethubsegment, solve the route
		if hubedge_row.edge_id <> -999 and hubedge_row.hoid <> -999 then
			--Check to see if they are the same arc - if so, we don't have to solve
		    if hubedge_row.edge_id <> segment_oid then 
    		--For the supplied segment objectid, iterate over the edge source and target nodeids from the fibercables table
		    	FOR srcnode_row IN 
		    		execute format ('select a.source as node from %I.%I a where a.edge_id = %s and a.source is not null
									union 
									select a.target as node from %I.%I a where a.edge_id = %s and a.target is not null',srcschema, l_edgetable, segment_oid,
																								srcschema, l_edgetable, segment_oid)
					--Solve the route for both ends, take the longest route......
				loop
					FOR hubnode_row in  			
						execute format ('select a.source as node from %I.%I a where a.edge_id = %s and a.source is not null
									union 
									select a.target as node from %I.%I a where a.edge_id = %s and a.target is not null',srcschema, l_edgetable, hubedge_row.edge_id,
																								srcschema, l_edgetable, hubedge_row.edge_id)
					loop 
						int_run:= int_run + 1;
						execute format('insert into tbl_routeresults(run, hubedge_id, segedge_id, edge_id, seq) select %s as run, %s, %s, a.edge_id, a.seq 
	    		            			from telecom.f_3gis_solveroute(%s, %s) a where a.seq <> -9999', int_run, hubedge_row.edge_id, segment_oid, hubnode_row.node, srcnode_row.node); 
--   		        result_cnt = result_cnt + 1;
					END loop;
					--Check to see if any of the potnetial routes _actually_ solved....
					SELECT count(*) INTO result_cnt FROM tbl_routeresults;
				END loop;
	        else
	            --Define single segment behavior...
	            return query (select f.fqn_id, l_site_span_nfid::varchar(50) as site_span_nfid, segment_oid as edge_id, 1 as seq 
           			from telecom.fibercable f where f.objectid = segment_oid);
           		RETURN;
	        end if;
	    --Attempt to find the cranhub failedm, use single segment behavior
	    else 
	    	return query (select 'Hub Missing'::varchar(100) AS fqn_id, f.site_span_nfid, segment_oid as edge_id, -999 as seq 
    			from telecom.fibercable f where f.objectid = segment_oid);
    		RETURN;
		end if;
	END loop;
	--Determine the edge_id of the hubsegment immediately upstream of the hub for the current solution - hubedge_id with highest number of occurences in the edge_id column in the output table
	with cte as
	(select count(r.*) as cnt, r.edge_id from tbl_routeresults r where r.edge_id in (select distinct hubedge_id from tbl_routeresults) group by r.edge_id)
	select cte.edge_id into hub_edgeid from cte where cte.cnt = (select max(cnt) from cte);
	-- Chekc to see if the results table has any records
	IF result_cnt > 0 THEN 
	--Find the shortest path originating / terminating at hubedge_id containing the edge_id	
	return query execute format(
	'with route_res as (select max(seq) as seq, run 
					from tbl_routeresults 
						where run in 
							(select run 
							from tbl_routeresults 
							where edge_id = %s 
								and hubedge_id = %s 
								and edge_id not in (select edge_id 
								from telecom.mv_coincidenthubsegments 
								where edge_id <> %s)) 
							group BY run)
	select b.fqn_id::varchar(50), %L::varchar(50) as site_span_nfid, a.edge_id::integer, a.seq::integer 
		from %I.%I b inner join tbl_routeresults a on b.objectid = a.edge_id 
		where a.run = (select run from route_res where  seq = (select max(seq) from route_res))',hub_edgeid, hub_edgeid, hub_edgeid,
																									l_site_span_nfid, 
	    																							srcschema, edge_parent);
	ELSE
		return query (select 'Connectivity Missing'::varchar(100) AS fqn_id, f.site_span_nfid, segment_oid as edge_id, -999 as seq 
    			from telecom.fibercable f where f.objectid = segment_oid);
	END IF;
end;
$threegis$
	language plpgsql;
