--Determine the line segment, hubid, and containg cranpolygonid of a route segment within a cranpolygon
drop function if exists telecom.f_3gis_gethubsegment(integer,text,text);
create or replace function telecom.f_3gis_gethubsegment( in segment_oid integer, in srcschema text  default 'telecom', in l_edgetable text default 'tbl_nfidroute')
returns table (
--l_target integer,
	edge_id integer,
	hoid integer,
	poid integer)
as
$threegis$
declare
	l_target integer;
	hoid integer;
	poid integer;
begin

	-- Find the nodeid of the 'start' hubpoint based on cranpolygon housing both
    --Get the containing mv_cranpolygon
    execute format ('select p.objectid from (select shape from %I.fibercable where objectid = %s) f, %I.mv_cranpolygons p 
    where st_contains(p.shape, st_centroid(f.shape))=true limit 1', srcschema, segment_oid, srcschema) into poid;
   raise notice 'Cran Polygon ID: %', poid;
   
   if poid is not null then
   		--Get the hub point
    	execute format ('select h.objectid from %I.cranhubs h, %I.mv_cranpolygons p 
    	where st_contains(p.shape, h.shape)=true and p.objectid = %s limit 1', srcschema, srcschema, poid) into hoid;
	    raise notice 'Hub ID: %', hoid;
	else 
		--No containing polygon
		return query (select -999 as edge_id, -999 as hoid, -999 as poid);
		return;
	end if;
    -- find the segments with dangles within 300 feet (90 meters) of the hub
    if hoid is not null THEN
    	 -- Get the union of the edges with a dangle with 100 meteers of the hub or have a vertex coincident with the hub
    	RETURN query 
	    SELECT DISTINCT a.edge_id, hoid, poid FROM (
	    	SELECT p.edge_id FROM telecom.mv_proximalhubsegments p WHERE p.objectid = hoid 
	    	UNION
			SELECT c.edge_id FROM telecom.mv_coincidenthubsegments c WHERE c.objectid = hoid) a;
	 else
	 	--Polygon does not have a cranhub
	 	return query (select -999 as l_target, -999 as edge_id, -999 as hoid, poid);
	 end if;
   
exception when others then
		RAISE INFO 'Error Name:%',SQLERRM;
		RAISE INFO 'Error State:%', SQLSTATE;
		raise info 'Association of Segment to Hub failed for Fibercable segment %, polygon %, hub %', segment_oid, poid, hoid ;
		RETURN query EXECUTE format ('SELECT %s AS edge_id, -999 AS hoid, -999 AS poid', segment_oid);
end;
$threegis$
	language plpgsql;
