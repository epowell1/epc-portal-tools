create or replace function telecom.f_3gis_nfid_updateroute()
returns trigger 
as 
$$
declare
	l_cnt integer;

begin
	if tg_op='INSERT' then 
		insert into telecom.tbl_nfidroute (geom, edge_id) values (new.shape, new.objectid);
	elsif tg_op='UPDATE' then
		--check to see if a record exists in the tbl_nfidroute table for the record
		select count(*) into l_cnt  from telecom.tbl_nfidroute where edge_id = new.objectid;
		raise notice 'Count: %', l_cnt;
	
		if l_cnt > 0 then
			update telecom.tbl_nfidroute set geom = new.shape where edge_id = new.objectid;
		else
			insert into telecom.tbl_nfidroute (geom, edge_id) values (new.shape, new.objectid);
		end if;

	else 
		delete from telecom.tbl_nfidroute where edge_id = old.objectid;
	end if;
	return null;
end;
$$
	language plpgsql;
