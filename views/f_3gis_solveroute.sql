drop function if exists f_3gis_solveroute(integer, integer,text,text);

create or replace function telecom.f_3gis_solveroute(sourceid integer, targetid integer,srcschema text default 'telecom', l_edgetable text  default 'tbl_nfidroute')
returns table (seq integer, edge_id integer )
as
$threegis$
begin
     return query execute format(' select seq, id2 as edge_id from 
                pgr_bddijkstra(''SELECT a.edge_id::INTEGER AS id,
                source::integer,
                target::integer,
                public.st_length(geom) as cost
                FROM %I.%I a''::text,
                %s::integer,
                %s::integer, 
                false,
                false)',srcschema, l_edgetable,
                        sourceid,
                        targetid);

exception
	when internal_error then
		RAISE INFO 'Error Name:%',SQLERRM;
		RAISE INFO 'Error State:%', SQLSTATE;
		raise INFO 'Could not solve route to Hub for Fibercable Sourcenode %, Targetnode %: Invalid topology', sourceid, targetid;                    
		RETURN query(SELECT -9999 AS seq, -9999 AS edge_id);
 end;
 $threegis$
 language plpgsql;
