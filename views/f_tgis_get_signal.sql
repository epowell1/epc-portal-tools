drop function  if exists tgis.f_tgis_getsignal(in_segmentid text, in_fiberid integer);
create or replace function tgis.f_tgis_getsignal(in_segmentid text, in_fiberid integer)
returns table (signalipid text,
	circuitname text,
	aequipmentipid text,
	astructureuniqueid text,
	astructurename text, 
	aequipmentname text,
	ablockname text,
	aportid integer,
	aportname text, 
	aporttype text,
	aportuniqueid text,
	zequipmentipid text,
	zstructureuniqueid text,
	zstructurename text, 
	zequipmentname text,
	zblockname text,
	zportid integer,
	zportname text,
	zporttype text,
	zportuniqueid text)
as 
$tgis$
declare
	the_signalipid text:=tgis.f_3gis_esriguid(gen_random_uuid()::text);
BEGIN	
	return query 
	with the_aside as (
		select 
			999 as join_val,
			coalesce (fp.circuitid, '-999') as acircuitname,
			sig.edgeid->>'aequipmentid' as aequipmentipid,
			'aaa' as astructureuniqueid,
			fe.structure_name as astructurename,
			fe.equipmentname as aequipmentname,
			fp.blockname as ablockname, 
			(sig.edgeid->>'aportid')::integer as aportid,
			fp.portname as aportname,
			(sig.edgeid->>'aporttype') as aporttype,
			fp.ipid as aportuniqueid
		from tgis.f_tgis_signalpath(in_segmentid, in_fiberid) sig 
		left join telecom.fiberport fp 
		on sig.edgeid->>'aequipmentid' = fp.equipmentipid 
		and (sig.edgeid->>'aportid')::integer = fp.portid 
		and (sig.edgeid->>'aporttype') = fp.porttype 
		left join telecom.fiberequipment fe 
		on sig.edgeid->>'aequipmentid' = fe.ipid
		where fp.portname  is not null),
	the_zside as(
		select 
			999 as join_val,
			coalesce (fp.circuitid, '-999') as zcircuitname,
			sig.edgeid->>'zequipmentid' as zequipmentipid,
			'zzz' as zstructureuniqueid,
			fe.structure_name as zstructurename,
			fe.equipmentname as zequipmentname,
			fp.blockname as zblockname,  
			(sig.edgeid->>'zportid')::integer as zportid,
			fp.portname as zportname,
			(sig.edgeid->>'zporttype') as zporttype,
			fp.ipid as zportuniqueid
		from tgis.f_tgis_signalpath(in_segmentid, in_fiberid) sig 
		left join telecom.fiberport fp 
		on sig.edgeid->>'zequipmentid' = fp.equipmentipid 
		and (sig.edgeid->>'zportid')::integer = fp.portid 
		and (sig.edgeid->>'zporttype') = fp.porttype 
		left join telecom.fiberequipment fe 
		on sig.edgeid->>'zequipmentid' = fe.ipid
		where fp.portname  is not null)
	select 
		the_signalipid as signalipid,
		coalesce (acircuitname , zcircuitname)::text as circuitname, 
		a.aequipmentipid,
		a.astructureuniqueid,
		a.astructurename::text, 
		a.aequipmentname::text,
		a.ablockname::text,
		a.aportid,
		a.aportname::text, 
		a.aporttype::text,
		a.aportuniqueid::text,
		z.zequipmentipid::text,
		z.zstructureuniqueid::text,
		z.zstructurename::text, 
		z.zequipmentname::text,
		z.zblockname::text,
		z.zportid,
		z.zportname::text, 
		z.zporttype::text,
		z.zportuniqueid::text 
		from the_aside a 
		full outer join the_zside z
		on a.join_val = z.join_val;
end;
$tgis$
language plpgsql;


