drop function if exists tgis.f_tgis_getsignalpath(startnode text);
create or replace function tgis.f_tgis_getsignalpath(startnode text)
returns table (pos integer, edgeid jsonb, signalipid text)
language plpgsql
as 
$tgis$
declare
	ipid text;
begin
	select tgis.f_3gis_esriguid(gen_random_uuid()::text) into ipid;
	--Generate signal based on any node in the topology.....
	--first chase downstream, get the downstream node and run in reverse to 
	--traverse upstream.
	return query execute format (
	'with nodechain as 
	(
		with ds_node as 
		(
			with ds as 
			(
				select * from connectby(''tgis.conn_edges'', ''targetoid'',  ''sourceoid'', ''targetoid'', %L, 0,''~'')
				AS t(keyid integer, parent_keyid integer, level int, branch text, pos int)
			)
			select keyid::integer 
			from ds
			where ds.pos = (select max(pos) from ds)
		)
		select t.* 
		from ds_node, connectby(''tgis.conn_edges'', ''sourceoid'', ''targetoid'', ''oid'',ds_node.keyid::text, 0,''~'')
		AS t(keyid integer, parent_keyid integer, level int, branch text, pos int)
	)
		select distinct pos, edgeid, %L as singalipid from 
		(select nc.pos as pos, ce.edgeid 
		from tgis.conn_nodes cn 
		inner join nodechain nc 
		on cn.node_id  = nc.keyid
		 join tgis.conn_edges ce 
		on cn.node_id  = ce.sourceoid
	--	union
	--	select nc.pos, ce.edgeid->>''segmentidfkey'' as segmentid, (ce.edgeid->>''fiberid'')::int as fiberid 
	--	from tgis.conn_nodes cn 
	--	inner join nodechain nc 
	--	on cn.node_id  = nc.keyid
	--	 join tgis.conn_edges ce 
	--	on cn.node_id  = ce.targetoid
		) r
		order by r.pos', startnode, ipid);
	return;
end;
$tgis$

