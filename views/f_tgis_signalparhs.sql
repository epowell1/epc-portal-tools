drop function if exists tgis.f_tgis_signalpath(in_segmentid text, in_fiberid integer);
create or replace function tgis.f_tgis_signalpath(in_segmentid text, in_fiberid integer)
returns table (edgeid jsonb, signalipid text)
language plpgsql
as 
$tgis$
declare
	nodeid text;
begin
--Get the starting node id from conn_edges based on segmentidfkey and fiberid
	select ce2.sourceoid::text into nodeid
	from tgis.conn_edges ce2 
		where ce2.edgeid->>'segmentidfkey' = in_segmentid 
		and (ce2.edgeid->>'fiberid')::integer = in_fiberid;
	return query select r.edgeid, r.signalipid 
				from tgis.f_tgis_getsignalpath(nodeid) r
				order by r.pos; 
return;
end;
$tgis$;




