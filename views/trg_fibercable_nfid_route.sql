--drop trigger if exists trg_fibercable;
create trigger trg_fibercableroute 
after INSERT or delete or update of shape, site_span_nfid 
on telecom.fibercable
for each row
when (pg_trigger_depth()=0)
execute procedure telecom.f_3gis_nfid_updateroute();
