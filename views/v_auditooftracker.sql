drop view telecom.v_nfid_status_dates;
create or replace view telecom.v_nfid_status_dates as
select o.sitespannfid, 
array_agg(f.fqn_id), o.status, 
max(o.constructionstartactual) as oof_constructionstartactual, 
max(o.cableplacedactual) as oof_cableplacedactual,
max(o.constructioncompletedactual) as oof_constructioncompletedactual
from telecom.oofsegmenttracker o 
inner join telecom.fibercable  f 
on f.nfid = o.sitespannfid
group by o.sitespannfid,  o.constructionstartactual, o.cableplacedactual ,o.constructioncompletedactual, o.status  ;


create or replace view telecom.v_auditoofftracker as
with nfids as 
(
	select max(f.cableplacedactual)::date as cableplacedactual, 
	max(f.constructionstartactual)::date as constructionstartactual, 
	max(f.splicetestactual )::date as splicetestactual, 
	f.nfid from telecom.fibercable f
	group by nfid
)
select vnsd.*  ,f.*
from telecom.v_nfid_status_dates vnsd 
inner join nfids f 
on f.nfid = vnsd.sitespannfid
where f.cableplacedactual<>vnsd.oof_cableplacedactual 
or 
f.constructionstartactual <> vnsd.oof_constructionstartactual;
