drop view telecom.v_auditoofftracker_fqnid ;
create or replace view telecom.v_auditoofftracker_fqnid as
select o.fqnid,
o.status,
o.constructionstartactual as oof_constrctionstart,
o.cableplacedactual as oof_cableplaced,
o.splicetestactual as oofsplicetest,
o.constructioncompletedactual as oof_constrctioncomplete,
f.constructionstartactual::date as f_constrctionstart,
f.cableplacedactual::date as f_cableplaced,
f.splicetestactual::date as f_splicetest
from telecom.oofsegmenttracker o 
inner join telecom.fibercable f 
on f.fqn_id = o.fqnid
where f.cableplacedactual::date<>o.cableplacedactual 
or 
f.constructionstartactual::date <> o.constructionstartactual;
