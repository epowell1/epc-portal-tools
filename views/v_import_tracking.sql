drop view if exists v_import_tracking ;

create or replace view tgis.v_import_tracking as
select filepath, market, v.vendor_name 
from tgis.import_tracking i 
inner join tgis.market_instance m 
on m.market_instance_id = i.market_id_fk
inner join tgis.vendors v 
on m.vendor_id_fk =v.vendor_id;