drop view if exists tgis.v_marketinfo;

create or replace view tgis.v_marketinfo as
select 
	mi.db_info->>'username' as user_name, 
	mi.db_info->>'password' as user_password, 
	mi.db_info->>'instance' as instance,
	mi.db_info->>'dbname' as dbname, 
	mi.db_info->>'port' as port,
	mi.filepath,
	v.vendor_name, 
	mi.market
from tgis.market_instance mi 
inner join tgis.vendors v  on 
v.vendor_id = mi.vendor_id_fk;



