--Create the route topology topology (~2 minutes)
SELECT pgr_createTopology('telecom.tbl_nfidroute', 2,'geom', 'edge_id');
    
--Check the route topology
SELECT pgr_analyzeGraph('telecom.tbl_nfidroute', 2, 'geom', 'edge_id');

--Refresh the pgr_vertices table
drop table if exists telecom.tbl_nfidroute_vertices_pgr cascade;
select pgr_createverticestable('telecom.tbl_nfidroute','geom','source','target','true');
--alter table telecom.tbl_nfidroute owner to telecom;

--Create the matierlized views for hub-segment associations
create materialized view telecom.mv_coincidenthubsegments as
with hubvertex as (select id, h.objectid from telecom.tbl_nfidroute_vertices_pgr v 
	inner join (select objectid, st_buffer(shape, 1) as the_geom from telecom.cranhubs) h
		on  st_intersects(v.the_geom, h.the_geom) = true) 
	select distinct a.edge_id, a.objectid from 
		(select edge_id, h.objectid 
		from telecom.tbl_nfidroute r 
		inner join hubvertex h
		on r.source = h.id union
select edge_id,h.objectid from telecom.tbl_nfidroute r inner join hubvertex h on r.target = h.id) a;

alter table telecom.mv_coincidenthubsegments owner to telecom;

create materialized view telecom.mv_proximalhubsegments as
with dangels as (
	with candidate as (select a.edge_id, node from (								
		with snglvrtx as (select id from telecom.tbl_nfidroute_vertices_pgr where cnt = 1)
		select edge_id, r.source as node from telecom.tbl_nfidroute r inner join snglvrtx on r.source = snglvrtx.id union
		select edge_id, r.target as node from telecom.tbl_nfidroute r inner join snglvrtx on r.target = snglvrtx.id) a)
	select c.edge_id, c.node from candidate c 
	inner join (select count(*) as cnt, edge_id 
		from candidate group by edge_id) a 
	on a.edge_id = c.edge_id where a.cnt =1)
select d.edge_id, h.objectid
	from (select objectid, st_buffer(shape,100) as the_geom from telecom.cranhubs) h 
	inner join (select pt.the_geom, dg.edge_id 
		from telecom.tbl_nfidroute_vertices_pgr pt 
			inner join dangels dg 
			on pt.id = dg.node) d 
	on st_intersects(h.the_geom, d.the_geom) = true;


alter table telecom.mv_proximalhubsegments owner to telecom;

create or replace view telecom.v_nfid_fqn_associations as
select distinct 
c.objectid,
c.site_nfid,
c.fqn_id
--c.operation_type,
--c.vendor_reference_id
from 
(select 
(telecom.f_3gis_getroute(a.objectid,'telecom','tbl_nfidroute','fibercable', 5)).edge_id::integer as objectid,
(telecom.f_3gis_getroute(a.objectid,'telecom','tbl_nfidroute','fibercable', 5)).site_span_nfid::varchar(12) as site_nfid,
(telecom.f_3gis_getroute(a.objectid,'telecom','tbl_nfidroute','fibercable', 5)).fqn_id::varchar(100),
--COALESCE(a.operation_type, 'I')::varchar(50) as operation_type,
--telecom.f_creationuser()::varchar(50) as vendor_reference_id,
(telecom.f_3gis_getroute(a.objectid,'telecom','tbl_nfidroute','fibercable', 5)).seq
from telecom.fibercable a) c
where c.objectid is not null and c.site_nfid is not null and c.fqn_id not like '%Missing%'
	order by c.site_nfid, c.fqn_id
